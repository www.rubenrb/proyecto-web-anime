<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anime | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/plyr.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body>

<?php

    session_start();
 

    $servername = "localhost";
    $username = "animeAdmin";
    $password = "animeAdmin";
    $dbname = "anime_db";

    $ultimos="ultimos";
    $populares="populares";
    $miembros="miembros";

    $conn = mysqli_connect($servername, $username, $password,$dbname);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    if (isset($_POST['salir'])){

      session_destroy();
   

      header("location:portfolio/login.php");
   }

   $user="";
   
    if (isset($_SESSION["conectado"])){

        $user=$_SESSION["conectado"];
    }

    if (isset($_POST['busqueda'])){

        $titulo=$_POST['titulo'];
        
        header("location:portfolio/categorias.php?categoria=$titulo&filtro=busqueda");

    }
?>


    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="index.php">
                            <img src="img/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li class="active"><a href="index.php">Inicio</a></li>
                                <li><a href="#">Categorias <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Action' ?>">Acción</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Martial_Arts' ?>">Artes marciales</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Sci-Fi' ?>">Ciencia ficción</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Comedy' ?>">Comedia</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Drama' ?>">Drama</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'School' ?>">Escolares</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Fantasy' ?>">Fantasia</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Horror' ?>">Horror</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Kids' ?>">Infantil</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Magic' ?>">Magia</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Mecha' ?>">Mecha</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Mystery' ?>">Misterio</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Music' ?>">Musical</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Parody' ?>">Parodia</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Slice_of_Life' ?>">Recuentos de la vida</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Romance' ?>">Romance</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Seinen' ?>">Seinen</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Shounen' ?>">Shounen</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Supernatural' ?>">Sobrenatural</a></li>
                                        <li><a href="portfolio/categorias.php?categoria=<?php echo 'Super_power' ?>">Super poderes</a></li>
                                    </ul>
                                </li>
                                <li><a href="portfolio/blog.php">Nuestro blog</a></li>
                                <li><a href="portfolio/contacto.php">Contactanos</a></li>
<?php
  
                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Usuario") {

?>
                                <li><a href="portfolio/perfil.php?user=<?php echo $user ?>">Perfil de usuario</a></li>
<?php

                            }

                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Admin") {

?>                              
                                <li><a href="portfolio/admin_menu.php">Administración</a></li>
<?php

                            }

?>

                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    
<?php
  
                    if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

?>
                        <div class="col-lg-6">
                            <div class="header__right">
                                <a href="portfolio/login.php"><span class="icon_profile"></span></a>
                            </div>
                        </div>
<?php

                    }else{   

?>
                        <div class="col-lg-6">
                            <form  action="#" method="post">
                                <button type="submit" class="site-btn3" name="salir">Salir</button>
                            </form>
                        </div>
<?php

                    }  

?>
                                 
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
    <!-- Header End -->

    <form  action="#" method="post">
        <div class="wrap">
           <div class="search">
              <input type="text" class="searchTerm" name="titulo" placeholder="Buscar..">
              <button type="submit" class="searchButton" name="busqueda">
                <i class="fa fa-search"></i>
             </button>
           </div>
        </div>
    </form><br>

    <!-- Hero Section Begin -->
    <section class="hero">
        <div class="container">
            <div class="hero__slider owl-carousel">
<?php
  
        $sql = "SELECT id,Titulo, SUBSTRING(Contenido, 1, 60),Foto, FechaNoticia FROM noticias ORDER BY id DESC LIMIT 3";
        $result = mysqli_query ($conn, $sql);

        if ($result == TRUE) {

            while ($registro = mysqli_fetch_row($result)) {

?>

                <div class="hero__items set-bg" data-setbg="<?php echo $registro[3] ?>">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="hero__text">
                                <div class="label">Blog - <?php echo $registro[4] ?></div>
                                <h2><?php echo $registro[1] ?></h2>
                                <p><?php echo $registro[2] ?>...</p>
                                <a href="portfolio/post_blog.php?id=<?php echo $registro[0] ?>"><span>Ver post completo</span> <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
<?php
            }
        }

?>

            </div>
        </div>
    </section>
    <!-- Hero Section End -->


    <!-- Product Section Begin -->
    <section class="product spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="trending__product">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <div class="section-title">
                                    <h4>Ultimos añadidos</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="btn__all">
                                    <a href="portfolio/categorias.php?categoria=<?php echo $ultimos ?>" class="primary-btn">Ver todos <span class="arrow_right"></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">

<?php
  
        $sql1 = "SELECT id,tittle,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist ORDER BY id DESC LIMIT 6";
        $result1 = mysqli_query ($conn, $sql1);

        if ($result1 == TRUE) {

            while ($registro = mysqli_fetch_row($result1)) {
              $ep= round($registro[3]);

?>

                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="product__item">
                                    <div class="product__item__pic set-bg" data-setbg="<?php echo $registro[6] ?>">
                                        <div class="ep"><?php echo $ep ?></div>
                                        <div class="comment"><i class="fa fa-comments"></i> <?php echo $registro[7] ?></div>
                                        <div class="view"><i class="fa fa-user"></i><?php echo $registro[4] ?></div>
                                    </div>
                                    <div class="product__item__text">
                                        <ul>
                                            <li><?php echo $registro[2]?></li>
                                        </ul>
                                        <h5><a href="portfolio/anime-details.php?id=<?php echo $registro[0] ?>"><?php echo $registro[1] ?></a></h5>
                                    </div>
                                </div>
                            </div>
                            
<?php
            }
        }

?>

                        </div>
                    </div>
                    <div class="popular__product">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <div class="section-title">
                                    <h4>Animes mas populares</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="btn__all">
                                    <a href="portfolio/categorias.php?categoria=<?php echo $populares ?>" class="primary-btn">Ver todos <span class="arrow_right"></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">

 <?php
  
        $sql1 = "SELECT tittle,id,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist ORDER BY score DESC LIMIT 6";
        $result1 = mysqli_query ($conn, $sql1);

        if ($result1 == TRUE) {

            while ($registro = mysqli_fetch_row($result1)) {
              $ep= round($registro[3]);

?>

                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="product__item">
                                    <div class="product__item__pic set-bg" data-setbg="<?php echo $registro[6] ?>">
                                        <div class="ep"><?php echo $ep ?></div>
                                        <div class="comment"><i class="fa fa-comments"></i> <?php echo $registro[7] ?></div>
                                        <div class="view"><i class="fa fa-user"></i><?php echo $registro[4] ?></div>
                                    </div>
                                    <div class="product__item__text">
                                        <ul>
                                            <li><?php echo $registro[2]?></li>
                                        </ul>
                                        <h5><a href="portfolio/anime-details.php?id=<?php echo $registro[1] ?>"><?php echo $registro[0] ?></a></h5>
                                    </div>
                                </div>
                            </div>
                            
<?php
            }
        }

?>


                        </div>
                    </div>
                    <div class="recent__product">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <div class="section-title">
                                    <h4>Animes con mas miembros</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="btn__all">
                                    <a href="portfolio/categorias.php?categoria=<?php echo $miembros ?>" class="primary-btn">Ver todos <span class="arrow_right"></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            

<?php
  
        $sql1 = "SELECT tittle,id,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist ORDER BY popularity ASC LIMIT 6";
        $result1 = mysqli_query ($conn, $sql1);

        if ($result1 == TRUE) {

            while ($registro = mysqli_fetch_row($result1)) {
              $ep= round($registro[3]);

?>

                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="product__item">
                                    <div class="product__item__pic set-bg" data-setbg="<?php echo $registro[6] ?>">
                                        <div class="ep"><?php echo $ep ?></div>
                                        <div class="comment"><i class="fa fa-comments"></i> <?php echo $registro[7] ?></div>
                                        <div class="view"><i class="fa fa-user"></i><?php echo $registro[4] ?></div>
                                    </div>
                                    <div class="product__item__text">
                                        <ul>
                                            <li><?php echo $registro[2]?></li>
                                        </ul>
                                        <h5><a href="portfolio/anime-details.php?id=<?php echo $registro[1] ?>"><?php echo $registro[0] ?></a></h5>
                                    </div>
                                </div>
                            </div>
                            
<?php
            }
        }

?>


                        </div>
                    </div>
                    <div class="live__product">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <div class="section-title">
                                    <h4>Recomendaciones de la web</h4>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            
                            
<?php
  
        $sql1 = "SELECT tittle,id,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist ORDER BY RAND() LIMIT 6";
        $result1 = mysqli_query ($conn, $sql1);

        if ($result1 == TRUE) {

            while ($registro = mysqli_fetch_row($result1)) {
              $ep= round($registro[3]);

?>

                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="product__item">
                                    <div class="product__item__pic set-bg" data-setbg="<?php echo $registro[6] ?>">
                                        <div class="ep"><?php echo $ep ?></div>
                                        <div class="comment"><i class="fa fa-comments"></i> <?php echo $registro[7] ?></div>
                                        <div class="view"><i class="fa fa-user"></i><?php echo $registro[4] ?></div>
                                    </div>
                                    <div class="product__item__text">
                                        <ul>
                                            <li><?php echo $registro[2]?></li>
                                        </ul>
                                        <h5><a href="portfolio/anime-details.php?id=<?php echo $registro[1] ?>"><?php echo $registro[0] ?></a></h5>
                                    </div>
                                </div>
                            </div>
                            
<?php
            }
        }

?>


                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-8">
                    <div class="product__sidebar">
                        <div class="product__sidebar__view">
                            <div class="section-title">
                                <h5>Top Vistas</h5>
                            </div>
                            <form  action="#" method="post">
                                <ul class="filter__controls">
                                    <li data-filter=".day">Día</li>
                                    <li data-filter=".week">Semana</li>
                                    <li data-filter=".month">Mes</li>
                                    <li data-filter=".years">Año</li>
                                </ul>
                            </form>

<?php

    
  
        $sql1 = "SELECT tittle,id,episodes,Visionados,img_url FROM myanimelist ORDER BY Visionados DESC LIMIT 4";
        $result1 = mysqli_query ($conn, $sql1);

        $sql2 = "SELECT tittle,id,episodes,Visionados_mes,img_url FROM myanimelist ORDER BY Visionados_mes DESC LIMIT 4";
        $result2 = mysqli_query ($conn, $sql2);

        $sql3 = "SELECT tittle,id,episodes,Visionados_semana,img_url FROM myanimelist ORDER BY Visionados_semana DESC LIMIT 4";
        $result3 = mysqli_query ($conn, $sql3);

        $sql4 = "SELECT tittle,id,episodes,Visionados_dia,img_url FROM myanimelist ORDER BY Visionados_dia DESC LIMIT 4";
        $result4 = mysqli_query ($conn, $sql4);
 
         /*$sql1 = "SELECT tittle,id,Visionados_semana FROM myanimelist WHERE Visionados_dia='0' ";

        Esta sentencia de sql se ejecuta solo cuando los valores de las filas son 0, una vez que se han rellenado conjunto al codigo de abajo ya no es necesario y se puede mantener desactivado*/


             if ($result == TRUE) {

                while ($registro = mysqli_fetch_row($result)) {

                /*$aleatorio=rand(1,$registro[2]);
                  $sql30 = " UPDATE myanimelist SET Visionados_dia='$aleatorio' WHERE  Visionados_dia='0' ";
                  $result30 = mysqli_query ($conn, $sql30);

                  Código para generar numeros aleatorios y que se añadan la fila de "visionados", simplemente para tener valores ficticios y poder trabajar con ellos */ 

?>

                                <div class="filter__gallery">
                                    <div class="product__sidebar__view__item set-bg mix years" 
                                    data-setbg="<?php echo $registro[4] ?>">
                                    <div class="ep"><?php echo $registro1[2] ?></div>
                                    <div class="view"><i class="fa fa-eye"></i><?php echo $registro[3] ?></div>
                                    <h5><a href="portfolio/anime-details.php?id=<?php echo $registro[1] ?>"><?php echo $registro[0] ?></a></h5>
                                </div>

<?php
                }

                
            }

            if ($result1 == TRUE) {

                while ($registro1 = mysqli_fetch_row($result1)) {

                /*$aleatorio=rand(1,$registro[2]);
                  $sql30 = " UPDATE myanimelist SET Visionados_dia='$aleatorio' WHERE  Visionados_dia='0' ";
                  $result30 = mysqli_query ($conn, $sql30);

                  Código para generar numeros aleatorios y que se añadan la fila de "visionados", simplemente para tener valores ficticios y poder trabajar con ellos */ 

?>

                                <div class="filter__gallery">
                                    <div class="product__sidebar__view__item set-bg mix years" 
                                    data-setbg="<?php echo $registro1[4] ?>">
                                    <div class="ep"><?php echo $registro1[2] ?></div>
                                    <div class="view"><i class="fa fa-eye"></i><?php echo $registro1[3] ?></div>
                                    <h5><a href="portfolio/anime-details.php?id=<?php echo $registro1[1] ?>"><?php echo $registro1[0] ?></a></h5>
                                </div>    

<?php
                }

                
            }
        
                if ($result2 == TRUE) {

                    while ($registro = mysqli_fetch_row($result2)) {

?>

                                    <div class="filter__gallery">
                                        <div class="product__sidebar__view__item set-bg mix month" 
                                        data-setbg="<?php echo $registro[4] ?>">
                                        <div class="ep"><?php echo $registro[2] ?></div>
                                        <div class="view"><i class="fa fa-eye"></i><?php echo $registro[3] ?></div>
                                        <h5><a href="portfolio/anime-details.php?id=<?php echo $registro[1] ?>"><?php echo $registro[0] ?></a></h5>
                                    </div> 

<?php
                    }
                }
        

        
            if ($result3 == TRUE ) {

                while ($registro = mysqli_fetch_row($result3)) {

?>

                               <div class="filter__gallery">
                                    <div class="product__sidebar__view__item set-bg mix week" 
                                    data-setbg="<?php echo $registro[4] ?>">
                                    <div class="ep"><?php echo $registro[2] ?></div>
                                    <div class="view"><i class="fa fa-eye"></i><?php echo $registro[3] ?></div>
                                    <h5><a href="portfolio/anime-details.php?id=<?php echo $registro[1] ?>"><?php echo $registro[0] ?></a></h5>
                                </div> 

<?php
                }
            }
        

        
            if ($result4 == TRUE ) {

                while ($registro = mysqli_fetch_row($result4)) {

?>

                                <div class="filter__gallery">
                                    <div class="product__sidebar__view__item set-bg mix day" 
                                    data-setbg="<?php echo $registro[4] ?>">
                                    <div class="ep"><?php echo $registro[2] ?></div>
                                    <div class="view"><i class="fa fa-eye"></i><?php echo $registro[3] ?></div>
                                    <h5><a href="portfolio/anime-details.php?id=<?php echo $registro[1] ?>"><?php echo $registro[0] ?></a></h5>
                                </div> 

<?php
                }
            }
    
        
?>

                            
            </div>
        </div>
    </div>
                <div class="product__sidebar__comment">
                    <div class="section-title">
                        <h5>Nuevos comentarios</h5>
                    </div>

<?php            


        
        $sql7 = "SELECT comentarios.id_comentario,comentarios.id,myanimelist.img_url,myanimelist.genre,myanimelist.tittle,myanimelist.Visionados FROM comentarios INNER JOIN myanimelist ON comentarios.id_comentario=myanimelist.id ORDER BY comentarios.id DESC LIMIT 4";
        $result7 = mysqli_query ($conn, $sql7);

        if ($result7 == TRUE) {

            while ($registro7 = mysqli_fetch_row($result7)) {
?>
                    <div class="product__sidebar__comment__item">
                        <div class="product__sidebar__comment__item__pic">
                            <img src="<?php echo $registro7[2] ?>" width="90" height="130" alt="nuevocomentario">
                        </div>
                        <div class="product__sidebar__comment__item__text">
                            <ul>
                                <li><?php echo $registro7[3] ?></li>
                            </ul>
                            <h5><a href="portfolio/anime-details.php?id=<?php echo $registro7[0] ?>"><?php echo $registro7[4] ?></a></h5>
                            <span><i class="fa fa-eye"></i><?php echo $registro7[5]?> Viewes</span>
                        </div>
                    </div>
                    
                
<?php 

            }
        }

?>
                </div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- Product Section End -->

<!-- Footer Section Begin -->
<footer class="footer">
    <div class="page-up">
        <a href="#" id="scrollToTopButton"><span class="arrow_carrot-up"></span></a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="footer__logo">
                    <a href="index.php"><img src="img/logo.png" alt=""></a>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="footer__nav">
                    <ul>
                        <li class="active"><a href="../index.php">Inicio</a></li>
                        <li><a href="portfolio/categorias.php?categoria=<?php echo $ultimos ?>">Categories</a></li>
                        <li><a href="portfolio/blog.php">Nuestro blog</a></li>
                        <li><a href="portfolio/contacto.php">Contacto</a></li>
                        <li><a href="portfolio/faq.php">FAQ</a></li>
                    </ul>
                </div>
            </div>
            
          </div>
      </div>
  </footer>
  <!-- Footer Section End -->


  <!-- Search model Begin -->
  <div class="search-model">
    <div class="h-100 d-flex align-items-center justify-content-center">
        <div class="search-close-switch"><i class="icon_close"></i></div>
        <form class="search-model-form">
            <input type="text" id="search-input" placeholder="Search here.....">
        </form>
    </div>
</div>
<!-- Search model end -->

<!-- Js Plugins -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/player.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/mixitup.min.js"></script>
<script src="js/jquery.slicknav.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/main.js"></script>


</body>

</html>