<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anime | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="../css/plyr.css" type="text/css">
    <link rel="stylesheet" href="../css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="../css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="../css/style.css" type="text/css">
</head>

<body>
<?php
 session_start();

 $user="";
   
    if (isset($_SESSION["conectado"])){

        $user=$_SESSION["conectado"];
    }

  function test_input($data) {
          $data = trim($data); 
          $data = stripslashes($data); 
         return $data;
    }

if(!isset($_SESSION["conectado"])) {

    $userError= $passError  = $loginError="";
    $user = $pass=  "";
    $usuario = $contra= false;

    if (isset($_POST['salir'])){

          session_destroy();
       

          header("location:login.php");
       }

    if ($_SERVER["REQUEST_METHOD"] == "POST") { 
      if (empty($_POST["user"])) {
          $userError = "* El campo Mail/Nombre es requerido <br>";
      }else{
          $user = test_input($_POST["user"]);
          $usuario = true;

      }

      


      if (empty($_POST["pass"])) {
          $passError = "* El campo Contraseña es requerido <br>";
      }else {
          $pass = test_input($_POST["pass"]);
          $contra = true;

      }

      

    }



  $servername = "localhost";
  $username = "animeAdmin";
  $password = "animeAdmin";
  $dbname = "anime_db";

  $conn = mysqli_connect($servername, $username, $password,$dbname);

  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }
  
    if(($usuario==true)&&($contra==true)){
        if (isset($_POST['ingresar'])){

              $str1=$_POST["user"];
              $pass=$_POST["pass"];
              $encript= hash_hmac('sha512', $pass, 'secret');
              $usero = strtolower($str1);
              $f= date("Y").'-'.date("m").'-'.date("d");
             
             

              $adm = "SELECT NombreUsuario, Pass, Estado,Mail, Rol FROM login WHERE (NombreUsuario='$usero' OR Mail='$usero') AND Pass='$encript' AND Estado='Activo' AND Rol='Admin'";
              $moderador = "SELECT NombreUsuario, Estado,Mail, Pass, Rol FROM login WHERE (NombreUsuario='$usero' OR Mail='$usero') AND Pass='$encript' AND Estado='Activo' AND Rol='Moderador'";
              $usuario = "SELECT NombreUsuario, Estado,Mail, Pass, Rol FROM login WHERE (NombreUsuario='$usero'  OR Mail='$usero') AND Pass ='$encript' AND Estado='Activo' AND Rol='Usuario'";
              $usuario2 = "SELECT NombreUsuario, Estado,Mail, Pass, Rol FROM login WHERE (NombreUsuario='$usero'  OR Mail='$usero') AND Pass ='$encript' AND Estado='Bloqueado' AND Rol='Usuario'";

              $result1 = mysqli_query ($conn, $adm);
              $result2 = mysqli_query ($conn, $moderador);
              $result3 = mysqli_query ($conn, $usuario);
              $result4 = mysqli_query ($conn, $usuario2);

            if(mysqli_num_rows($result1) > 0){
              
              session_start();
              $registro = mysqli_fetch_row($result1);
              $_SESSION['rol']=$registro[4];
              $_SESSION['conectado']=$registro[0];
              $sql1 = " UPDATE login SET UltimaConexion='$f' WHERE (NombreUsuario='$usero' OR Mail='$usero')";
              $result5 = mysqli_query ($conn, $sql1);
              var_dump($result);

              header("Location: ../index.php");
     
              exit();
            }
            if(mysqli_num_rows($result2) > 0){
              session_start();
              $registro = mysqli_fetch_row($result2);
              $_SESSION['rol']=$registro[4];
              $_SESSION['conectado']=$registro[0];
              $sql2 = " UPDATE login SET UltimaConexion='$f' WHERE (NombreUsuario='$usero' OR Mail='$usero')";
              $result6 = mysqli_query ($conn, $sql2);

              header("Location: ../index.php");
     
              exit();
            }
            if(mysqli_num_rows($result3) > 0){
              session_start();
              $registro = mysqli_fetch_row($result3);
              $_SESSION['rol']=$registro[4];
              $_SESSION['conectado']=$registro[0];
              $sql3 = " UPDATE login SET UltimaConexion='$f' WHERE (NombreUsuario='$usero' OR Mail='$usero')";
              $result7 = mysqli_query ($conn, $sql3);

                header("Location: ../index.php");
              exit();
            }

            if(mysqli_num_rows($result4) > 0){
             
             $loginError = "* Este usuario se encuentra bloqueado de nuestra web por incumplir las reglas de comportamiento <br>";
              
            }

            else{

                $loginError = "* El usuario/mail o contraseña no es valido <br>";
            }   

        }
    }
    
       mysqli_close($conn); 

  ?>


    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="../index.php">
                            <img src="../img/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li><a href="../index.php">Inicio</a></li>
                                <li><a href="#">Categorias <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="categorias.php?categoria=<?php echo 'Action' ?>">Acción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Martial_Arts' ?>">Artes marciales</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Sci-Fi' ?>">Ciencia ficción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Comedy' ?>">Comedia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Drama' ?>">Drama</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'School' ?>">Escolares</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Fantasy' ?>">Fantasia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Horror' ?>">Horror</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Kids' ?>">Infantil</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Magic' ?>">Magia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mecha' ?>">Mecha</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mystery' ?>">Misterio</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Music' ?>">Musical</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Parody' ?>">Parodia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Slice_of_Life' ?>">Recuentos de la vida</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Romance' ?>">Romance</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Seinen' ?>">Seinen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Shounen' ?>">Shounen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Supernatural' ?>">Sobrenatural</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Super_power' ?>">Super poderes</a></li>
                                    </ul>
                                </li>
                                <li><a href="blog.php">Nuestro blog</a></li>
                                <li><a href="contacto.php">Contactanos</a></li>
<?php
  
                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Usuario") {

?>
                                <li><a href="perfil.php?user=<?php echo $user ?>">Perfil de usuario</a></li>
<?php

                            }

                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Admin") {

?>                              
                                <li><a href="admin_menu.php">Administración</a></li>
<?php

                            }

?>

                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    
<?php
  
                    if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

?>
                        <div class="col-lg-6">
                            <div class="header__right">
                                <a href="login.php"><span class="icon_profile"></span></a>
                            </div>
                        </div>
<?php

                    }else{   

?>
                        <div class="col-lg-6">
                            <form  action="" method="post">
                                <button type="submit" class="site-btn3" name="salir">Salir</button>
                            </form>
                        </div>
<?php

                    }  

?>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Normal Breadcrumb Begin -->
    <section class="normal-breadcrumb set-bg" data-setbg="../img/normal-breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="normal__breadcrumb__text">
                        <h2>Login</h2>
                        <p>Bienvenido a Anime Mix.</p>
                    </div>
                </div>
            </div>
        </div>
    </section><br><br><br><br><br><br>
    <!-- Normal Breadcrumb End -->

    <!-- Login Section Begin -->
    <section class="login spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="login__form">
                        <h3>Login</h3>
                        <form action="" method="post">
                            <span id="rojo"> <?php echo $loginError;?></span>
                            <span id="rojo"> <?php echo $userError;?></span>
                            <span id="rojo"> <?php echo  $passError;?></span><br>
                            <div class="input__item">
                                <input type="text" name="user" placeholder="Introduce tu mail o usuraio">
                                <span class="icon_mail">
                            </div>
                                </span>
                            <div class="input__item">
                                <input type="text" name="pass" placeholder="Contraseña">
                                <span class="icon_lock">
                            </div>
                            </span>
                            <button type="submit" class="site-btn" name="ingresar">Iniciar sesisón</button>
                        </form>
                        <a href="recover_password.php" class="forget_pass">¿Olvidaste tu contraseña?</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="login__register">
                        <h3>¿No tienes cuenta?</h3>
                        <a href="signup.php" class="primary-btn">Registrate ahora</a>
                    </div>
                </div>
            </div>
            <div class="login__social">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-6">
                        <div class="login__social__links">
                            <span>también puedes con:</span>
                            <ul>
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i> Sign in With
                                Facebook</a></li>
                                <li><a href="#" class="google"><i class="fa fa-google"></i> Sign in With Google</a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i> Sign in With Twitter</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Login Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer">
        <div class="page-up">
            <a href="#" id="scrollToTopButton"><span class="arrow_carrot-up"></span></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer__logo">
                        <a href="../index.php"><img src="../img/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="footer__nav">
                        <ul>
                            <li class="active"><a href="../index.php">Inicio</a></li>
                            <li><a href="categorias.php?categoria=<?php echo 'ultimos' ?>">Categories</a></li>
                            <li><a href="blog.php">Nuestro blog</a></li>
                            <li><a href="contacto.php">Contacto</a></li>
                            <li><a href="faq.php">FAQ</a></li>
                        </ul>
                    </div>
                </div>
                
              </div>
          </div>
      </footer>
      <!-- Footer Section End -->




      <!-- Search model Begin -->
      <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch"><i class="icon_close"></i></div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

    <!-- Js Plugins -->
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/player.js"></script>
    <script src="../js/jquery.nice-select.min.js"></script>
    <script src="../js/mixitup.min.js"></script>
    <script src="../js/jquery.slicknav.js"></script>
    <script src="../js/owl.carousel.min.js"></script>
    <script src="../js/main.js"></script>

<?php  
       

}else{

    header("location:perfil.php?user=$user");

}

?>

</body>

</html>