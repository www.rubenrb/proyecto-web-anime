<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anime | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">


    <!-- Css Styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="../css/plyr.css" type="text/css">
    <link rel="stylesheet" href="../css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="../css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="../css/style.css" type="text/css">
</head>

<body>
<?php
  
  session_start();

if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Admin") {

  $servername = "localhost";
  $username = "animeAdmin";
  $password = "animeAdmin";
  $dbname = "anime_db";

  if (isset($_POST['salir'])){

      session_destroy();
   

      header("location:login.php");
   }

  

   $conn = mysqli_connect($servername, $username, $password,$dbname);

  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }
  
    $error1=false;
    $subido="";
    $error="";

  $error3=false;
  $error2=$subido1="";


  if (isset($_POST['subir'])){

    if(empty($_POST["title"]) || empty($_POST["foto"]) || empty($_POST["contenido"])  ) {
        $error = "* Debe completar todos los campos <br>";
    }else{

        $error1 = true;
       
    }
  }



    if(($error1==true)){
      if (isset($_POST['subir'])){

        $f= date("Y").'-'.date("m").'-'.date("d");

        $title=$_POST["title"];
        $foto=$_POST["foto"];
        $contenido=$_POST["contenido"];
        
        $sql= "INSERT INTO noticias (id,Titulo,Contenido,foto,FechaNoticia) VALUES (NULL,'$title','$contenido','$foto','$f')";
         
         $result = mysqli_query ($conn, $sql);

          
        if ($result == FALSE) {
           
              echo "Error en la ejecución de la consulta.<br />";
              
        }else{
              
              $subido = " Se ha subido el post correctamente <br>"; 

        }

      }
    }

    if (isset($_POST['postear'])) {


            if(empty($_POST["titulo"]) || empty($_POST["sinopsis"]) || empty($_POST["genero"]) || empty($_POST["fecha"]) || empty($_POST["episodios"]) || empty($_POST["imagen"]) || empty($_POST["link"]) ) {

                $error2 = "* Debe completar todos los campos <br>";
            }else{

                $error3 = true;
       
            }

            if(($error3==true)){
           

                    $titulo=$_POST["titulo"];
                    $sinopsis=$_POST["sinopsis"];
                    $genero=$_POST["genero"];
                    $fecha=$_POST["fecha"];
                    $episodios=$_POST["episodios"];
                    $imagen=$_POST["imagen"];
                    $link=$_POST["link"];

                    $uid=rand(20000,50000);
            
                $sql6000= "INSERT INTO myanimelist (id,uid,tittle,synopsis,genre,aired,episodes,members,popularity,ranked,score,img_url,link) VALUES (NULL,'$uid','$titulo','$sinopsis','$genero','$fecha','$episodios','0','0','0','0','$imagen','$link')";
             
                $result6000 = mysqli_query ($conn,$sql6000);

                var_dump($result6000);
                var_dump($sql6000);

              
                if ($result6000 == FALSE) {
                   
                      echo "Error en la ejecución de la consulta.<br />";
                      
                }else{
                      
                      $subido1 = " Se ha subido el post correctamente <br>"; 

                }

            }
    


        }
  
  ?>


    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="../index.php">
                            <img src="../img/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li><a href="../index.php">Inicio</a></li>
                                <li><a href="#">Categorias <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="categorias.php?categoria=<?php echo 'Action' ?>">Acción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Martial_Arts' ?>">Artes marciales</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Sci-Fi' ?>">Ciencia ficción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Comedy' ?>">Comedia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Drama' ?>">Drama</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'School' ?>">Escolares</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Fantasy' ?>">Fantasia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Horror' ?>">Horror</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Kids' ?>">Infantil</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Magic' ?>">Magia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mecha' ?>">Mecha</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mystery' ?>">Misterio</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Music' ?>">Musical</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Parody' ?>">Parodia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Slice_of_Life' ?>">Recuentos de la vida</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Romance' ?>">Romance</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Seinen' ?>">Seinen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Shounen' ?>">Shounen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Supernatural' ?>">Sobrenatural</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Super_power' ?>">Super poderes</a></li>
                                    </ul>
                                </li>
                                <li><a href="blog.php">Nuestro blog</a></li>
                                <li><a href="contacto.php">Contactanos</a></li>
<?php
  
                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Usuario") {

?>
                                <li><a href="perfil.php?user=<?php echo $user ?>">Perfil de usuario</a></li>
<?php

                            }

                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Admin") {

?>                              
                                <li class="active"><a href="admin_menu.php">Administración</a></li>
<?php

                            }

?>

                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    
<?php
  
                    if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

?>
                        <div class="col-lg-6">
                            <div class="header__right">
                                <a href="login.php"><span class="icon_profile"></span></a>
                            </div>
                        </div>
<?php

                    }else{   

?>
                        <div class="col-lg-6">
                            <form  action="" method="post">
                                <button type="submit" class="site-btn3" name="salir">Salir</button>
                            </form>
                        </div>
<?php

                    }  

?>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Normal Breadcrumb Begin -->
    <section class="normal-breadcrumb set-bg" data-setbg="../img/normal-breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="normal__breadcrumb__text">
                        <h2>Panel de control de administración</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Normal Breadcrumb End -->

    <!-- Login Section Begin -->
    <section class="login spad">
        <div>
           
                <div >
                   <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3 align="center">Menu de administración</h3>
            </div>
        <form action="" method="post">
            <ul class="list-unstyled components">
                <h4 align="center">Opciones</h4><br>
                    <button type="submit" class="site-btn1" name="anadir">Añadir entrada al blog</button>
                </li>
                <li>
                    <button type="submit" class="site-btn2" name="nuevo">Añadir nuevo anime</button>
                </li>
                <li>
                    <button type="submit" class="site-btn1" name="restablecer">Restablecer contraseña</button>
                </li>
                <li>
                    <button type="submit" class="site-btn2" name="activo">Lista de usuarios Activos</button>
                </li>
                <li>
                    <button type="submit" class="site-btn1" name="bloqueados">Lista de Usuarios bloqueados</button>
                </li>
                <li>
                    <button type="submit" class="site-btn2" name="limpiar">Limpieza de usuarios</button>
                </li>
                <li>
                    <button type="submit" class="site-btn1" name="mensaje">Mensajes de la web</button>
                </li>
                <li>
                    <button type="submit" class="site-btn2" name="msgadmin">Enviar mensaje de administración</button>
                </li>
                
            </ul>
        </form>
          
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>Desplegar / ocultar menú</span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>

                    
                </div>
            </nav>

<?php
        if (isset($_POST['anadir']) || (isset($_POST['subir']))) {

?>


        <section >
        <div class="container">
            <div class="login__form1">
                        <h3 class="contacto">Panel para añadir una nueva entrada al blog</h3>
                        <span id="amarillo">-Para el contenido del post se usaran las etiquetas html "p" para cada parrafo que se quiera añadir</span><br>
                        <span id="amarillo">-Si se quiere insrtar una imagen en el post debe ir dentro de las etiquetas html "br", "center", y "img" en ese orden para que la web pueda insertar el contenido adecuadamente desde la base de datos y quede estructurado.</span><br>
                        <span id="amarillo">-Para remarcar alguna parte del texto utilizar la etiqueta html "strong"</span><br>
                        <form action="" method="post">
                            <br><br><span id="rojo"> <?php echo $error;?></span>
                            <span id="amarillo"> <?php echo $subido;?></span><br>
                            
                            <div class="input__item">
                                <input type="text" name="title" placeholder="Titulo del post">
                                <span class="icon_search_alt">
                            </div>
                                </span>
                                <div class="input__item">
                                <input type="url" name="foto" placeholder="URL de la imagen">
                                <span class="icon_images">
                            </div>
                                </span>
                            <div class="input__item1">
                                <textarea  name="contenido" rows="6" cols="64" placeholder="Contenido del post"></textarea>
                            </div>
                           
                            
                           
                            <button type="submit" class="site-btn1" name="subir">Subir post</button>
                        </form>
            </div>
                
            
        </div>
        </section>

<?php

        }

       

        if (isset($_POST['restablecer']) || (isset($_POST['actualizar']))|| (isset($_POST['atras1'])) || (isset($_POST['cambiar']))) {

            $sql10 = "SELECT NombreUsuario, Estado,Mail, Rol FROM login";
            $result10 = mysqli_query ($conn, $sql10);

            if ($result10 == TRUE) {
    ?>
                
                <table cellspacing="0" cellpadding="0" class="user-table">
                   <tr  id="user-table-top">
                      <th>
                         <h3>Usuario</h3>
                      </th>
                      <th>
                         <h3>Email</h3>
                      </th>
                      <th>
                         <h3>Estado</h3>
                      </th>
                      <th>
                         <h3>Rol</h3>
                      </th>
                      <th>
                         <h3>Restablecer</h3>
                      </th>
                      
                      
                   </tr>

<?php
                    while ($registro = mysqli_fetch_row($result10)) {
?>
                    <form action="" method="post">
                    <div class="form-group row justify-content-center">    
                        <input name="datos" type="hidden" value="<?php echo $registro[0] ?>">
                       <tr>
                          <th>
                             <h5><?php echo $registro[0] ?></h5>
                          </th>
                          <th>
                             <h5><?php echo $registro[2] ?></h5>
                          </th>
                          <th>
                             <h5><?php echo $registro[1] ?></h5>
                          </th>
                          <th>
                             <h5><?php echo $registro[3] ?></h5>
                          </th>
                          
                              <th colspan="2" style="text-align:center;"><button type="submit" name="editar"><i class="fas fa-edit" name="editar"></i></button></th>
                          </th>
                       </tr>
                    </div>
                    </form>

<?php
        
                    }
?>
   
                </table>
            
<?php
            }

        }

        if (isset($_POST['editar'])) {

            $datos=$_POST["datos"];

            $sql20 = "SELECT NombreUsuario, Mail, Estado FROM login where NombreUsuario='$datos' ";
                  $result20 = mysqli_query ($conn, $sql20);

                  if(mysqli_num_rows($result20) > 0){
                    while ($registro = mysqli_fetch_row($result20)) {

                   
                      $_SESSION['dato']=$registro[1];
                      $_SESSION['dato1']=$registro[0];

                    

              ?>  
                        <section >
                            <div class="container">
                                <div class="login__form2">
                                  <h3>Escribe la nueva contraseña</h3><br>

                                  <p><b>Usuario:</b> <?php echo $registro[0];?></p>
                                  
                                  <p><b>Mail: </b> <?php echo $registro[1];?></p>
                                  
                                  <p><b>Estado: </b> 
                                    <?php
                                        echo $registro[2] ."<br>";

                                        if($registro[2]=="Bloqueado"){

                                            echo "<p>A este usuario le ha sido restringido el acceso</p>";   
                                        }
                                   ?>
                                  </p>
                                  
                                  <form action="" method="post">
                                    <div id="centro">
                                        <p><b>Nueva contraseña</b></p> <input name="pass" type="text">
                                        <br><br>
                                
                                        <input class="btn btn-danger" type="submit" value="Modificar"  name="cambiar">
                                        <input class="btn btn-danger" type="reset" name="Submit" value="Reiniciar formulario">
                                        <input class="btn btn-danger" type="submit" value="Volver atras"  name="atras1">
                                    </div>

                                  </form>
                                </div>
                            </div>
                        </section >


              <?php           
                  
                    }
                  } 

        }



        if (isset($_POST['cambiar'])) {

            $pass=$_POST["pass"];
            $mail=$_SESSION['dato'];
            $nombreUsu=$_SESSION['dato1'];
            $encript= hash_hmac('sha512', $pass, 'secret');
            $sql30 = " UPDATE login SET Pass='$encript' WHERE Mail='$mail' ";
            $result30 = mysqli_query ($conn, $sql30);


            if ($result30 == FALSE) {
                echo "Error en la ejecución de la consulta.<br />";
            }else{
                echo "<div class='login__form2'>";
               echo "<p>Se ha modificado la contraseña con exito</p>";
                echo "<div>";
            }


            $para      = $mail;
            $asunto    = 'Solicitud cambio de contraseña';
            $descripcion   = 'Se ha modificado la contraseña para el usuario: '. $nombreUsu ."\nNueva Contraseña:  ".$pass;
             
                
            $de = 'From: Anime Mix';

            if (mail($para, $asunto, $descripcion, $de)){
                $enviado = " Tu mensaje se ha enviado correctamente <br>"; 
            }   

            

        }      


        


        if (isset($_POST['activo']) || (isset($_POST['atras2']))) {

            $sql40 = "SELECT NombreUsuario, Estado,Mail, Rol FROM login WHERE Estado='Activo'";
            $result40 = mysqli_query ($conn, $sql40);

            if ($result40 == TRUE) {
    ?>
                
                <table cellspacing="0" cellpadding="0" class="user-table">
                   <tr  id="user-table-top">
                      <th>
                         <h3>Usuario</h3>
                      </th>
                      <th>
                         <h3>Email</h3>
                      </th>
                      <th>
                         <h3>Estado</h3>
                      </th>
                      <th>
                         <h3>Rol</h3>
                      </th>
                      <th>
                         <h3>Bloquear usuario</h3>
                      </th>
                      
                      
                   </tr>

<?php
                    while ($registro = mysqli_fetch_row($result40)) {
?>
                    <form action="" method="post">
                    <div class="form-group row justify-content-center">    
                        <input name="datos" type="hidden" value="<?php echo $registro[0] ?>">
                        <input name="mail" type="hidden" value="<?php echo $registro[2] ?>">

                       <tr>
                          <th>
                             <h5><?php echo $registro[0] ?></h5>
                          </th>
                          <th>
                             <h5><?php echo $registro[2] ?></h5>
                          </th>
                          <th>
                             <h5><?php echo $registro[1] ?></h5>
                          </th>
                          <th>
                             <h5><?php echo $registro[3] ?></h5>
                          </th>
                          
                              <th colspan="2" style="text-align:center;"><button type="submit" name="bloquear"><i class="fas fa-user-times" name="bloquear"></i></button></th>

                          </th>
                       </tr>
                    </div>
                    </form>

<?php
        
                    }
?>
   
                </table>
            
<?php
            }

        }

       
        if (isset($_POST['bloquear'])) {

            $datos=$_POST["datos"];
            $mail=$_POST["mail"];

            $sql50 = " UPDATE login SET Estado='Bloqueado' WHERE NombreUsuario='$datos' ";
            $result50 = mysqli_query ($conn, $sql50);


            if ($result50 == FALSE) {
                echo "Error en la ejecución de la consulta.<br />";
            }else{

                $sql60 = "SELECT NombreUsuario, Estado,Mail, Rol FROM login WHERE Estado='Activo'";
                $result60 = mysqli_query ($conn, $sql60);

                if ($result60 == TRUE) {
        ?>
                    
                    <table cellspacing="0" cellpadding="0" class="user-table">
                       <tr  id="user-table-top">
                          <th>
                             <h3>Usuario</h3>
                          </th>
                          <th>
                             <h3>Email</h3>
                          </th>
                          <th>
                             <h3>Estado</h3>
                          </th>
                          <th>
                             <h3>Rol</h3>
                          </th>
                          <th>
                             <h3>Bloquear usuario</h3>
                          </th>
                          
                          
                       </tr>

    <?php
                        while ($registro = mysqli_fetch_row($result60)) {
    ?>
                        <form action="" method="post">
                        <div class="form-group row justify-content-center">    
                            <input name="datos" type="hidden" value="<?php echo $registro[0] ?>">
                            <input name="mail" type="hidden" value="<?php echo $registro[2] ?>">

                           <tr>
                              <th>
                                 <h5><?php echo $registro[0] ?></h5>
                              </th>
                              <th>
                                 <h5><?php echo $registro[2] ?></h5>
                              </th>
                              <th>
                                 <h5><?php echo $registro[1] ?></h5>
                              </th>
                              <th>
                                 <h5><?php echo $registro[3] ?></h5>
                              </th>
                              
                                  <th colspan="2" style="text-align:center;"><button type="submit" name="bloquear"><i class="fas fa-user-times" name="bloquear"></i></button></th>

                              </th>
                           </tr>
                        </div>
                        </form>

    <?php
            
                        }
    ?>
       
                    </table>
                
    <?php
                }
                echo "<div class='login__form2'>";
               echo "<p>Se ha bloqueado el usuario con exito</p>";
                echo "<div>";


                $para      = $mail;
                $asunto    = 'Cuenta bloqueada';
                $descripcion   = 'Se ha bloqueado tu cuenta por incumplir las normas de nuestra comunidad.' ."\nSi tiene alguna duda puede ponerse en contacto a traves del siguiente correo: foroprueba93@gmail.com ";
             
                
                $de = 'From: Anime Mix';

                if (mail($para, $asunto, $descripcion, $de)){
                    $enviado = " Tu mensaje se ha enviado correctamente <br>"; 
                }   
            }


            

        }  


       

            if (isset($_POST['bloqueados']) || (isset($_POST['atras3']))) {

            $sql400 = "SELECT NombreUsuario, Estado,Mail, Rol FROM login WHERE Estado='Bloqueado'";
            $result400 = mysqli_query ($conn, $sql400);

            if ($result400 == TRUE) {
    ?>
                
                <table cellspacing="0" cellpadding="0" class="user-table">
                   <tr  id="user-table-top">
                      <th>
                         <h3>Usuario</h3>
                      </th>
                      <th>
                         <h3>Email</h3>
                      </th>
                      <th>
                         <h3>Estado</h3>
                      </th>
                      <th>
                         <h3>Rol</h3>
                      </th>
                      <th>
                         <h3>Desbloquear usuario</h3>
                      </th>
                      
                      
                   </tr>

<?php
                    while ($registro = mysqli_fetch_row($result400)) {
?>
                    <form action="" method="post">
                    <div class="form-group row justify-content-center">    
                        <input name="datos" type="hidden" value="<?php echo $registro[0] ?>">
                        <input name="mail" type="hidden" value="<?php echo $registro[2] ?>">

                       <tr>
                          <th>
                             <h5><?php echo $registro[0] ?></h5>
                          </th>
                          <th>
                             <h5><?php echo $registro[2] ?></h5>
                          </th>
                          <th>
                             <h5><?php echo $registro[1] ?></h5>
                          </th>
                          <th>
                             <h5><?php echo $registro[3] ?></h5>
                          </th>
                          
                              <th colspan="2" style="text-align:center;"><button type="submit" name="desbloquear"><i class="fas fa-edit" name="desbloquear"></i></button></th>

                          </th>
                       </tr>
                    </div>
                    </form>

<?php
        
                    }
?>
   
                </table>
            
<?php
            }

        }

       
        if (isset($_POST['desbloquear'])) {

            $datos=$_POST["datos"];
            $mail=$_POST["mail"];

            $sql500 = " UPDATE login SET Estado='Activo' WHERE NombreUsuario='$datos' ";
            $result500 = mysqli_query ($conn, $sql500);


            if ($result500 == FALSE) {
                echo "Error en la ejecución de la consulta.<br />";
            }else{

                $sql600 = "SELECT NombreUsuario, Estado,Mail, Rol FROM login WHERE Estado='Bloqueado'";
                $result600 = mysqli_query ($conn, $sql600);

                if ($result600 == TRUE) {
        ?>
                    
                    <table cellspacing="0" cellpadding="0" class="user-table">
                       <tr  id="user-table-top">
                          <th>
                             <h3>Usuario</h3>
                          </th>
                          <th>
                             <h3>Email</h3>
                          </th>
                          <th>
                             <h3>Estado</h3>
                          </th>
                          <th>
                             <h3>Rol</h3>
                          </th>
                          <th>
                             <h3>Desbloquear usuario</h3>
                          </th>
                          
                          
                       </tr>

    <?php
                        while ($registro = mysqli_fetch_row($result600)) {
    ?>
                        <form action="" method="post">
                        <div class="form-group row justify-content-center">    
                            <input name="datos" type="hidden" value="<?php echo $registro[0] ?>">
                            <input name="mail" type="hidden" value="<?php echo $registro[2] ?>">

                           <tr>
                              <th>
                                 <h5><?php echo $registro[0] ?></h5>
                              </th>
                              <th>
                                 <h5><?php echo $registro[2] ?></h5>
                              </th>
                              <th>
                                 <h5><?php echo $registro[1] ?></h5>
                              </th>
                              <th>
                                 <h5><?php echo $registro[3] ?></h5>
                              </th>
                              
                                  <th colspan="2" style="text-align:center;"><button type="submit" name="desbloquear"><i class="fas fa-edit" name="desbloquear"></i></button></th>

                              </th>
                           </tr>
                        </div>
                        </form>

    <?php
            
                        }
    ?>
       
                    </table>
                
    <?php
                }
                echo "<div class='login__form2'>";
               echo "<p>Se ha desbloqueado el usuario con exito</p>";
                echo "<div>";


                $para      = $mail;
                $asunto    = 'Cuenta desbloqueada';
                $descripcion   = 'Se te ha permitido el accesso de nuevo a tu cuenta.' ."\nTen cuidado y respeta las normas de la comunidad para no volver a ser baneado.";
             
                
                $de = 'From: Anime Mix';

                if (mail($para, $asunto, $descripcion, $de)){
                    $enviado = " Tu mensaje se ha enviado correctamente <br>"; 
                }   
            }


            

        } 


        if (isset($_POST['limpiar']) || (isset($_POST['inactivo']))) {

            $f= date("Y").'-'.date("m").'-'.date("d");

            $sql1000 = " UPDATE login SET fechaActual='$f' WHERE Estado='Activo'";
            $result1000 = mysqli_query ($conn, $sql1000);

            

            $sql2000="SELECT NombreUsuario,DATEDIFF(fechaActual, UltimaConexion) FROM login  WHERE Estado='Activo'";
            $result2000 = mysqli_query ($conn, $sql2000);


            while ($registro = mysqli_fetch_row($result2000)) {

                $user=$registro[0];
                $dias=$registro[1];

             $sql3000 = " UPDATE login SET DiasDesconectado='$dias' WHERE (NombreUsuario='$user')";
             $result3000 = mysqli_query ($conn, $sql3000);

            }



            $sql4000="SELECT NombreUsuario,Estado,Rol,UltimaConexion,DiasDesconectado FROM login  WHERE Estado='Activo' AND DiasDesconectado>365";
            $result4000 = mysqli_query ($conn, $sql4000);


            if ($result4000 == TRUE) {
?>
                    
                    <table cellspacing="0" cellpadding="0" class="user-table">
                       <tr  id="user-table-top">
                          <th>
                             <h3>Usuario</h3>
                          </th>
                          <th>
                             <h3>Estado</h3>
                          </th>
                          <th>
                             <h3>Rol</h3>
                          </th>
                          <th>
                             <h3>Ultima Conexion</h3>
                          </th>
                          <th>
                             <h3>Dias Desconectado</h3>
                          </th>

                          
                          
                       </tr>

<?php
                
                while ($registro = mysqli_fetch_row($result4000)) {
                    
?>
                        <form action="" method="post">
                        <div class="form-group row justify-content-center">    

                           <tr>
                              <th>
                                 <h5><?php echo $registro[0] ?></h5>
                              </th>
                              <th>
                                 <h5><?php echo $registro[1] ?></h5>
                              </th>
                              <th>
                                 <h5><?php echo $registro[2] ?></h5>
                              </th>
                              <th>
                                 <h5><?php echo $registro[3] ?></h5>
                              </th>
                              <th style="text-align:center;">
                                 <h5><?php echo $registro[4] ?> Días</h5>
                              </th>
                              
                           </tr>
                           <button type="submit" class="site-btn1" name="inactivo">Eliminar usuarios inactivos</button>
                        </div>

                        </form>

<?php
            
                }
?>
       
                    </table>
                
<?php
            }
        }

        if (isset($_POST['inactivo'])) {

           $sql5000="DELETE FROM login WHERE DiasDesconectado>365";
           $result5000 = mysqli_query ($conn, $sql5000);

           if ($result5000 == TRUE) {
            echo "<div class='login__form2'>";
            echo "<p>Se ha limpiado la lista con exito</p>";
            echo "<div>";

           }
        }


        if (isset($_POST['nuevo']) || (isset($_POST['postear']))) {

?>


        <section >
        <div class="container">
            <div class="login__form1">
                        <h3 class="contacto">Panel para añadir un anime nuevo a la base de datos</h3>
                        <span id="amarillo">-Para las sinopsis se usaran las etiquetas html "p" para cada parrafo que se quiera añadir</span><br>
                        <span id="amarillo">-El genero de cada anime se incluira dentro de "[]" entrecomillado por las comillas simples, en caso de incluir varios generos estos seran separados por comas .</span><br>
                        <span id="amarillo">-La fecha de publicacion y finalizacion de cada anime tendra el siguiente formato (Oct 4, 2015 to Mar 27, 2016)</span><br>
                        <span id="amarillo">-La imagen y la ficha correspondiente se obtendra desde la web de myanimelist</span><br>
                        <form action="" method="post">
                            <br><br><span id="rojo"> <?php echo $error2;?></span>
                            <span id="amarillo"> <?php echo $subido1;?></span><br>
                            
                            <div class="input__item">
                                <input type="text" name="titulo" placeholder="Titulo del Anime">
                                <span class="icon_document_alt">
                            </div>
                                </span>
                            <div class="input__item1">
                                <textarea  name="sinopsis" rows="6" cols="64" placeholder="Escribe la sinopsis"></textarea>
                            </div>
                                <div class="input__item">
                                <input type="text" name="genero" placeholder="Genero">
                                <span class="icon_clipboard">
                            </div>
                                </span>

                                <div class="input__item">
                                <input type="text" name="fecha" placeholder="Fecha inicio / Fecha finalización">
                                <span class="icon_calendar">
                            </div>
                                </span>

                                 <div class="input__item">
                                <input type="number" name="episodios" placeholder="Número de episodios">
                                <span class="icon_tag_alt">
                            </div>
                                </span>

                                <div class="input__item">
                                <input type="url" name="imagen" placeholder="URL de la imagen">
                                <span class="icon_images">
                            </div>
                                </span>

                                <div class="input__item">
                                <input type="url" name="link" placeholder="Enlace a la ficha de myanimelist">
                                <span class="icon_link">
                            </div>
                                </span>
                            
                           
                            
                           
                            <button type="submit" class="site-btn1" name="postear">Subir ficha de anime</button>
                        </form>
            </div>                
            
        </div>
        </section>

<?php

        }

        if (isset($_POST['mensaje']) || (isset($_POST['atras4']))) {

          $sql1 = "SELECT id, Emisor, Asunto, Fecha, Leido FROM mensajes WHERE Receptor = 'web'";

          

          $result = mysqli_query ($conn, $sql1);

            
            if ($result == FALSE) {
                echo "Error en la ejecución de la consulta.<br />";
            }else {
?>

            <section >
                <div class="container">
                    <form  action="#" method="post">
<?php
                        echo "<div class='tabla'>";
                            echo "<table style='width:100%'>";
                                echo "<tr>";
                                    echo "<th>"."Seleccion el mensaje que quieras leer"." <br>";
                                    echo "<th>" ."Asunto "."</th>";
                                    echo "<th>" ."Mensaje de "."</th>";
                                    echo "<th>" ." Leído  "."</th>";
                                    echo "<th>" ." Fecha  "."</th>";
                                echo "</tr>";

                while ($registro = mysqli_fetch_row($result)) {
                
                                echo "<tr>";
?>
                                    <td><input type="checkbox" name="checkList[]" value="<?php echo $registro[0] ?>"></td>
<?php
                                    echo "<td>" .$registro[2]."</td>";
                                    echo "<td>" .$registro[1]."</td>";
                                    echo "<td>" .$registro[4]."</td>";
                                    echo "<td>" .$registro[3]."</td>";
                                echo "</tr>";

                }
                        echo "</table>"."<br>";
                    echo "</div>";
?>
                        <br><br>
                        <div align="center">

                            <button class="site-btn1" type="submit" name="vermsg"> Leer mensaje</button>
                            <button class="site-btn1" type="submit" name="borra"> Borrar mensaje</button>
                        </div>
                    </form>
                </div>
            </section >

<?php                   
           }
        }
?>
        <section >
            <div class="container">
<?php  
                if (isset($_POST['vermsg'])){
                    if (isset($_POST['checkList'])) {
                        $cuenta=count($_POST['checkList']);

                        if ($cuenta==1){

                            foreach ($_POST['checkList'] as $selected) {
                                $sql2 = "SELECT Mensaje, Emisor, Asunto, Fecha FROM mensajes WHERE id = '$selected'";
                                $result1 = mysqli_query ($conn, $sql2);

                                $sql20 = "UPDATE mensajes SET Leido='Si' WHERE id = '$selected'";
                                $result10 = mysqli_query ($conn, $sql20);

                                if ($result10 == FALSE){
                                    echo "Error en la ejecución de la consulta.<br />";
                                }   
                                
                                if ($result1 == FALSE){
                                    echo "Error en la ejecución de la consulta.<br />";
                                }else {
                   
                                    while ($registro = mysqli_fetch_row($result1)) {
                    
                                        $_SESSION["respuesta"]=$registro[1];

                                        echo "<div class='caja'>";
                                            echo "<br><br><br>";
                                            echo "<b>Asunto:</b>"." ".$registro[2]."<br>";
                                            echo "<b>Mensaje de: </b>"." " . $registro[1]."<br>";
                                            echo "<b>Mensaje: </b> " ." ";
                                            echo $registro[0]."<br>";
                                            echo "<b>Fecha del mensaje:</b> "." " . $registro[3];
                                            echo "<br><br><br>";
                                        echo "</div>";
                   
?>
                                        <form  action="#" method="post">
                                            <div align="center">
                                                <button class="site-btn1" type="submit" name="atras4"> Volver atras</button>      
                                            </div>
                                        </form>
<?php 
                                    }
                      
                                }

                            }
                        }else{
                            echo "<br><br>";
                            echo "<h3 class='titulo'>Escoge solo un mensaje para leer.</h3><br>";

?>
                            <form  action="#" method="post">
                                <div align="center"> 
                                    <button class="site-btn1" type="submit" name="atras4"> Volver atras</button>
                                </div>
                            </form>
<?php
                        }
                    }else{
                        echo "<br><br>";
                        echo "<h3 class='titulo'>Debes escoger almenos un mensaje para leer.</h3><br>";
?>
                        <form  action="#" method="post">
                            <div align="center"> 
                                <button class="site-btn1" type="submit" name="atras4"> Volver atras</button>
                            </div>
                        </form>
<?php
                    }
                }
?>
        
            </div>
        </section >
<?php 

                if (isset($_POST['borra'])){
                    if (isset($_POST['checkList'])) {
                        $cuenta=count($_POST['checkList']);

                        if ($cuenta==1){

                            foreach ($_POST['checkList'] as $selected1) {
                                $sql3 = "DELETE from mensajes where id='$selected1'";
                                $result2 = mysqli_query ($conn, $sql3);

                                $sql1 = "SELECT id, Emisor, Asunto, Fecha, Leido FROM mensajes WHERE Receptor = 'web'";

                                $result = mysqli_query ($conn, $sql1);
            
                                if ($result == FALSE) {
                                    echo "Error en la ejecución de la consulta.<br />";
                                }else {
?>
                                    <section >
                                        <div class="container">
                                            <form  action="#" method="post">
<?php
                                                echo "<div class='tabla'>";
                                                    echo "<table style='width:100%'>";
                                                        echo "<tr>";
                                                            echo "<th>"."Seleccion el mensaje que quieras leer"." <br>";
                                                            echo "<th>" ."Asunto "."</th>";
                                                            echo "<th>" ."Mensaje de "."</th>";
                                                            echo "<th>" ." Leído  "."</th>";
                                                            echo "<th>" ." Fecha  "."</th>";
                                                        echo "</tr>";

                                        while ($registro = mysqli_fetch_row($result)) {
                                        
                                                        echo "<tr>";
?>
                                                            <td><input type="checkbox" name="checkList[]" value="<?php echo $registro[0] ?>"></td>
<?php
                                                            echo "<td>" .$registro[2]."</td>";
                                                            echo "<td>" .$registro[1]."</td>";
                                                            echo "<td>" .$registro[4]."</td>";
                                                            echo "<td>" .$registro[3]."</td>";
                                                        echo "</tr>";

                                        }
                                                    echo "</table>"."<br>";
                                                echo "</div>";
?>
                                                <br><br>
                                                    <div align="center">

                                                        <button class="site-btn1" type="submit" name="vermsg"> Leer mensaje</button>
                                                        <button class="site-btn1" type="submit" name="borra"> Borrar mensaje</button>
                                                    </div>
                                            </form>
                                        </div>
                                    </section >

<?php                   
                                }if ($result2 == FALSE) {
                                    echo "Error en la ejecución de la consulta.<br />";
                                }
                            }
                        }else{
                            echo "<br><br>";
                            echo "<h3 class='titulo'>Escoge solo un mensaje para borrar.</h3><br>";

?>
                            <form  action="#" method="post">
                                <div align="center"> 
                                    <button class="site-btn1" type="submit" name="atras4"> Volver atras</button>
                                </div>
                            </form>
                          
<?php
                        }
                    }else{
                        echo "<br><br>";
                        echo "<h3 class='titulo'>Debes escoger almenos un mensaje para borrar.</h3><br>";
?>
                        <form  action="#" method="post">
                            <div align="center"> 
                                <button class="site-btn1" type="submit" name="atras4"> Volver atras</button>
                            </div>
                        </form>
                          
<?php
                    }
                }


        if (isset($_POST['msgadmin']) || (isset($_POST['atras5']))) {

?>
            <div id="texto">
                <section >
                    <div class="container">
                        <form action="#" method="post">

                           <b>Asunto</b><br><br> <input id="miid" name="asunto" type="text" placeholder="Asunto del mensaje" align="center"><br><br>
                           
                           <b>Destinatario</b><br><br> <input id="miid"  name="destinatario" type="text" placeholder="contacto@correo.com"><br><br>
                           
                            <b>Mensaje:</b><br><br> <textarea name="comment" rows="5" cols="40"></textarea>
                           <br><br>
                            <button class="site-btn1" type="submit" name="enviomsg"> Enviar mensaje</button>
                        </form>
                    </div>
                </section >
            </div>

<?php                   
           
        }


        if (isset($_POST['enviomsg'])){

            $f= date("Y").'-'.date("m").'-'.date("d").' '.date("h").':'.date("i").':'.date("s");

            $asunto=$_POST["asunto"];
            $destino=$_POST["destinatario"];
            $mensaje=$_POST["comment"];

            $sql100 = "SELECT Mail FROM login WHERE Mail = '$destino'";

            $result100 = mysqli_query ($conn, $sql100);

            
            while ($registro = mysqli_fetch_row($result100)){

                $valor3=$registro[0];

                if($valor3==$destino){
                    
                    $sql2= "INSERT INTO mensajes (id,Receptor,Emisor,Mensaje,Asunto,Leido,Fecha) VALUES (NULL,'$destino','Admin web','$mensaje','$asunto','No','$f')";
              

                    $result2 = mysqli_query ($conn, $sql2);
                    if ($result2 == FALSE) {
                            echo "Error en la ejecución de la consulta.<br />";

                    }
                    if ($result2 == TRUE) {
                            echo "<h3 class='titulo1'>El mensaje se ha enviado con exito.</h3><br>";
?>
                            <form  action="#" method="post">
                                <div align="center"> 
                                    <button class="site-btn1" type="submit" name="atras5"> Volver atras</button>
                                </div>
                            </form>
                          
<?php
                    }
                    
                }

            }
        }
?>

    
        </div>
        </div>     
        </div>       
        </div>
    </section>
    <!-- Login Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer">
        <div class="page-up">
            <a href="#" id="scrollToTopButton"><span class="arrow_carrot-up"></span></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer__logo">
                        <a href="../index.php"><img src="../img/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="footer__nav">
                        <ul>
                            <li class="active"><a href="../index.php">Inicio</a></li>
                            <li><a href="categorias.php?categoria=<?php echo 'ultimos' ?>">Categories</a></li>
                            <li><a href="blog.php">Nuestro blog</a></li>
                            <li><a href="contacto.php">Contacto</a></li>
                            <li><a href="faq.php">FAQ</a></li>
                        </ul>
                    </div>
                </div>
                
              </div>
          </div>
      </footer>
      <!-- Footer Section End -->


      <!-- Search model Begin -->
      <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch"><i class="icon_close"></i></div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

    <!-- Js Plugins -->
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/player.js"></script>
    <script src="../js/jquery.nice-select.min.js"></script>
    <script src="../js/mixitup.min.js"></script>
    <script src="../js/jquery.slicknav.js"></script>
    <script src="../js/owl.carousel.min.js"></script>
    <script src="../js/main.js"></script>

<?php

}else{

    header("location:login.php");

}

?>


</body>

</html>