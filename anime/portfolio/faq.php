<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anime | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="../css/plyr.css" type="text/css">
    <link rel="stylesheet" href="../css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="../css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="../css/style.css" type="text/css">
</head>

<body>
<?php
  
  session_start();


   $user="";
   
    if (isset($_SESSION["conectado"])){

        $user=$_SESSION["conectado"];
    }
  
  $servername = "localhost";
  $username = "animeAdmin";
  $password = "animeAdmin";
  $dbname = "anime_db";

  if (isset($_POST['salir'])){

      session_destroy();
   

      header("location:login.php");
   }

   $conn = mysqli_connect($servername, $username, $password,$dbname);

  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }
  
  $enviado="";
  $email1=false;
  $emailerr="";


  if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $mail=$_POST["email"];

    if(empty($_POST["email"])) {
        $emailerr = "* Email necesario <br>";
    }else{
         if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            
            $email1 = true;
        }else{
            $emailerr = "* Introduce un mail válido <br>";
        }
    }

    

  }

  if(($email1==true)){
      if (isset($_POST['solicitar'])){

        
        $email=$_POST["email"];
       
        
         $sql = " SELECT NombreUsuario,Mail FROM login WHERE Mail='$email'";

         $result = mysqli_query($conn, $sql);

          
        if(mysqli_num_rows($result) > 0){

                $registro = mysqli_fetch_row($result);
                $user=$registro[0];
                $mail=$registro[1];

                $para      = 'foroprueba93@gmail.com';
                $asunto    = 'Solicitud cambio de contraseña';
                $descripcion   = 'Nombre de usuario: '. $user ."\nMail:  ".$mail;
             
                
                $de = 'From: Anime Mix';

                if (mail($para, $asunto, $descripcion, $de)){
                    $enviado = " Tu mensaje se ha enviado correctamente <br>"; 
                }   
              
        }else{

                $enviado = " No se ha encontrado ninguna cuenta con el mail introducido. <br>"; 

        }

      }
    }

  
  ?>


    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="../index.php">
                            <img src="../img/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li><a href="../index.php">Inicio</a></li>
                                <li><a href="#">Categorias <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="categorias.php?categoria=<?php echo 'Action' ?>">Acción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Martial_Arts' ?>">Artes marciales</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Sci-Fi' ?>">Ciencia ficción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Comedy' ?>">Comedia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Drama' ?>">Drama</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'School' ?>">Escolares</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Fantasy' ?>">Fantasia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Horror' ?>">Horror</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Kids' ?>">Infantil</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Magic' ?>">Magia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mecha' ?>">Mecha</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mystery' ?>">Misterio</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Music' ?>">Musical</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Parody' ?>">Parodia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Slice_of_Life' ?>">Recuentos de la vida</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Romance' ?>">Romance</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Seinen' ?>">Seinen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Shounen' ?>">Shounen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Supernatural' ?>">Sobrenatural</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Super_power' ?>">Super poderes</a></li>
                                    </ul>
                                </li>
                                <li><a href="blog.php">Nuestro blog</a></li>
                                <li><a href="contacto.php">Contactanos</a></li>
<?php
  
                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Usuario") {

?>
                                <li><a href="perfil.php?user=<?php echo $user ?>">Perfil de usuario</a></li>
<?php

                            }

                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Admin") {

?>                              
                                <li><a href="admin_menu.php">Administración</a></li>
<?php

                            }

?>

                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    
<?php
  
                    if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

?>
                        <div class="col-lg-6">
                            <div class="header__right">
                                <a href="login.php"><span class="icon_profile"></span></a>
                            </div>
                        </div>
<?php

                    }else{   

?>
                        <div class="col-lg-6">
                            <form  action="" method="post">
                                <button type="submit" class="site-btn3" name="salir">Salir</button>
                            </form>
                        </div>
<?php

                    }  

?>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Normal Breadcrumb Begin -->
    <section class="normal-breadcrumb set-bg" data-setbg="../img/normal-breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="normal__breadcrumb__text">
                        <h2>FAQ</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Normal Breadcrumb End -->

    <!-- Login Section Begin -->
    <section class="login spad">
        <div class="container">
            <div class="faq-header">Preguntas más frecuentes</div>

                <div class="faq-content">
                  <div class="faq-question">
                    <input id="q1" type="checkbox" class="panelfaq">
                    <div class="plus">+</div>
                    <label for="q1" class="panel-title">¿Que es <strong>AnimeMix</strong>?</label>
                    <div class="panel-content">AnimeMix es una web en la que puedes tener archivados todos tus progresos en las series o peliculas de anime que vayas viendo.</div>
                  </div>
                  
                  <div class="faq-question">
                    <input id="q2" type="checkbox" class="panelfaq">
                    <div class="plus">+</div>
                    <label for="q2" class="panel-title">¿Que es <strong>"anime"</strong>?</label>
                    <div class="panel-content">El anime es una animación que se produce y se concibe principalmente en Japón. Esto incluye la animación concebida en Japón que tiene partes subcontratadas a otros países, pero no incluye la animación producida principalmente fuera de Japón y con la animación subcontratada a Japón. Sin embargo, incluye animación co-concebida dentro y fuera de Japón y producida al menos parcialmente dentro de Japón.</div>
                  </div>
                  
                  <div class="faq-question">
                    <input id="q4" type="checkbox" class="panelfaq">
                    <div class="plus">+</div>
                    <label for="q4" class="panel-title">¿Con qué frecuencia actualiza la web el contenido?</label>
                    <div class="panel-content">El sitio se ejecuta en un formato de contenido diario, en lugar de un formato de revista mensual o bimensual. Actualizamos las noticias y reseñas a diario, y también se agrega otro contenido, como características, columnas y editoriales a lo largo de la semana.</a></div>
                  </div>

                  <div class="faq-question">
                    <input id="q5" type="checkbox" class="panelfaq">
                    <div class="plus">+</div>
                    <label for="q5" class="panel-title">¿Qué se utilizó para hacer el sitio, así como para mantenerlo?</label>
                    <div class="panel-content">El sitio es una combinación de PHP, Bootstrap (HTML y CSS), con una base de datos MySQL para administrar el contenido.</a></div>
                  </div>

                  <div class="faq-question">
                    <input id="q6" type="checkbox" class="panelfaq">
                    <div class="plus">+</div>
                    <label for="q6" class="panel-title">¿Cómo puedo ponerme en contacto con el personal de la web?</label>
                    <div class="panel-content">Visite nuestra <a href="contacto.php" style="color:#FF0000;">página de contacto</a> más información de contacto y personal.</a></div>
                  </div>

                  <div class="faq-question">
                    <input id="q7" type="checkbox" class="panelfaq">
                    <div class="plus">+</div>
                    <label for="q7" class="panel-title">¿Cómo debo enviar noticias?</label>
                    <div class="panel-content">Envíe un correo electrónico a foroprueba93@gmail.com , esta dirección de correo electrónico es monitoreada regularmente por varios miembros del personal (siempre hay alguien "de servicio").</a></div>
                  </div>

                  <div class="faq-question">
                    <input id="q8" type="checkbox" class="panelfaq">
                    <div class="plus">+</div>
                    <label for="q8" class="panel-title">Envié una noticia, ¿por qué no se publicó?</label>
                    <div class="panel-content">Todavía podríamos estar en el proceso de verificar los detalles, es posible que no hayamos podido verificar los elementos, o tal vez no lo encontramos lo suficientemente digno de ser publicado. En un día de noticias "lento", es más probable que publiquemos artículos menos importantes.</a></div>
                  </div>

                  <div class="faq-question">
                    <input id="q9" type="checkbox" class="panelfaq">
                    <div class="plus">+</div>
                    <label for="q9" class="panel-title">Envié una noticia, ¿por qué se le atribuyó a otra persona?</label>
                    <div class="panel-content">Lo enviaron primero, o lo que es más importante, enviaron más información.</a></div>
                  </div>

                  <div class="faq-question">
                    <input id="q10" type="checkbox" class="panelfaq">
                    <div class="plus">+</div>
                    <label for="q10" class="panel-title">¿Puedo enviar noticias sobre una animación no japonesa?</label>
                    <div class="panel-content">No. Somos una web de animación japonesa, no una web de animación en general o de animación asiatica. Hay mucha buena animación de todo el mundo, incluidos Corea y China, pero solo cubrimos la animación japonesa, no la animación francesa, mexicana, china, africana, coreana o afgana.</a></div>
                  </div>

                  <div class="faq-question">
                    <input id="q11" type="checkbox" class="panelfaq">
                    <div class="plus">+</div>
                    <label for="q11" class="panel-title">¿Puedo enviar un artículo?</label>
                    <div class="panel-content">Si usted es un escritor / periodista experimentado con un portafolio que puede señalar, envíe un correo electrónico a los editores y pregunte sobre el tema, estaremos encantados de considerar la idea de su artículo.</a></div>
                  </div>

                  <div class="faq-question">
                    <input id="q12" type="checkbox" class="panelfaq">
                    <div class="plus">+</div>
                    <label for="q12" class="panel-title">¿Puedo enviar noticias sobre una animación no japonesa?</label>
                    <div class="panel-content">No. Somos una web de animación japonesa, no una web de animación en general o de animación asiatica. Hay mucha buena animación de todo el mundo, incluidos Corea y China, pero solo cubrimos la animación japonesa, no la animación francesa, mexicana, china, africana, coreana o afgana.</a></div>
                  </div>

                  <div class="faq-question">
                    <input id="q13" type="checkbox" class="panelfaq">
                    <div class="plus">+</div>
                    <label for="q13" class="panel-title">¿Puedo cambiar mi nombre de usuario?</label>
                    <div class="panel-content">Puede realizar cambios en la configuración de su cuenta de usuario. Pero debido a que otros usuarios dependen de su nombre de usuario para reconocerlo, tenemos una política de "no cambio de identidad": una vez que comienza a usar la web , no permitimos cambios en su nombre de usuario.</a></div>
                  </div>

                  <div class="faq-question">
                    <input id="q14" type="checkbox" class="panelfaq">
                    <div class="plus">+</div>
                    <label for="q14" class="panel-title">¿Por que se ha eliminado mi cuenta?</label>
                    <div class="panel-content">Por motivos de seguridad y almacenamiento de datos damos un margen de un año a todas las cuentas que han quedado inactivas antes de proceder a su eliminación.</a></div>
                  </div>

                  <div class="faq-question">
                    <input id="q15" type="checkbox" class="panelfaq">
                    <div class="plus">+</div>
                    <label for="q15" class="panel-title">Me han bloqueado el acceso a mi cuenta, ¿que puedo hacer?</label>
                    <div class="panel-content">Debido a nuestras politicas de buena conducta no permitimos abusos o insultos hacia otros usuarios de la web, castigando al autor con la pena maxima de la web, baneo permanente. Es por esto que no se permitira el acceso de nuevo a nuestra web.</a></div>
                  </div>

                </div>
            </div>
        </div>
    </section>
    <!-- Login Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer">
        <div class="page-up">
            <a href="#" id="scrollToTopButton"><span class="arrow_carrot-up"></span></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer__logo">
                        <a href="../index.php"><img src="../img/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="footer__nav">
                        <ul>
                            <li class="active"><a href="../index.php">Inicio</a></li>
                            <li><a href="categorias.php?categoria=<?php echo 'ultimos' ?>">Categories</a></li>
                            <li><a href="blog.php">Nuestro blog</a></li>
                            <li><a href="contacto.php">Contacto</a></li>
                            <li><a href="faq.php">FAQ</a></li>
                        </ul>
                    </div>
                </div>
                
              </div>
          </div>
      </footer>
      <!-- Footer Section End -->


      <!-- Search model Begin -->
      <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch"><i class="icon_close"></i></div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

    <!-- Js Plugins -->
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/player.js"></script>
    <script src="../js/jquery.nice-select.min.js"></script>
    <script src="../js/mixitup.min.js"></script>
    <script src="../js/jquery.slicknav.js"></script>
    <script src="../js/owl.carousel.min.js"></script>
    <script src="../js/main.js"></script>


</body>

</html>