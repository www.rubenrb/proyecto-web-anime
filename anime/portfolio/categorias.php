<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anime | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="../css/plyr.css" type="text/css">
    <link rel="stylesheet" href="../css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="../css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="../css/style.css" type="text/css">
</head>

<body>

<?php
  session_start();

    $servername = "localhost";
    $username = "animeAdmin";
    $password = "animeAdmin";
    $dbname = "anime_db";

    $cat=$_GET['categoria'];

    if(isset($_GET['filtro'])){

        $filtro=$_GET['filtro'];
    }


     if(!isset($_GET['categoria'])){

        header("location:../index.php");
    }


    $conn = mysqli_connect($servername, $username, $password,$dbname);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    if (isset($_POST['salir'])){

      session_destroy();
   

      header("location:login.php");
   }

   $mensaje1="";
   $user="";
   
    if (isset($_SESSION["conectado"])){

        $user=$_SESSION["conectado"];
    }

    $record_per_page = 15;
    $pagina = '';

    if(isset($_GET["pagina"])){
        $pagina = $_GET["pagina"];
    }
    else{

        $pagina = 1;
    }

    $start_from = ($pagina-1)*$record_per_page;
    


    if (isset($_POST['buscar'])){

        $genero1=$_POST['genero1'];
        $genero2=$_POST['genero2'];
        $orden=$_POST['orden'];

        if (!empty($genero1) && !empty($genero2)){

            $mensaje1="Selecciona solo un genero";

        }

        if (empty($genero1) && empty($genero2)){

            $mensaje1="Selecciona al menos un genero";

        }

        if (!empty($_POST['genero1']) && empty($_POST['genero2']) && !empty($orden)){

            header("location:categorias.php?categoria=$genero1&filtro=$orden");

        }

        if (!empty($_POST['genero2']) && empty($_POST['genero1']) && !empty($orden)){

            header("location:categorias.php?categoria=$genero2&filtro=$orden");

        }

    }

    if (isset($_POST['busqueda'])){

        $titulo=$_POST['titulo'];
        
        header("location:categorias.php?categoria=$titulo&filtro=busqueda");

    }

?>


    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="../index.php">
                            <img src="../img/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li><a href="../index.php">Inicio</a></li>
                                <li class="active"><a href="#">Categorias <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="categorias.php?categoria=<?php echo 'Action' ?>">Acción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Martial_Arts' ?>">Artes marciales</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Sci-Fi' ?>">Ciencia ficción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Comedy' ?>">Comedia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Drama' ?>">Drama</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'School' ?>">Escolares</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Fantasy' ?>">Fantasia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Horror' ?>">Horror</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Kids' ?>">Infantil</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Magic' ?>">Magia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mecha' ?>">Mecha</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mystery' ?>">Misterio</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Music' ?>">Musical</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Parody' ?>">Parodia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Slice_of_Life' ?>">Recuentos de la vida</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Romance' ?>">Romance</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Seinen' ?>">Seinen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Shounen' ?>">Shounen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Supernatural' ?>">Sobrenatural</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Super_power' ?>">Super poderes</a></li>
                                    </ul>
                                </li>
                                <li><a href="blog.php">Nuestro blog</a></li>
                                <li><a href="contacto.php">Contactanos</a></li>
<?php
  
                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Usuario") {

?>
                                <li><a href="perfil.php?user=<?php echo $user ?>">Perfil de usuario</a></li>
<?php

                            }

                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Admin") {

?>                              
                                <li><a href="admin_menu.php">Administración</a></li>
<?php

                            }

?>

                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    
<?php
  
                    if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

?>
                        <div class="col-lg-6">
                            <div class="header__right">
                                <a href="login.php"><span class="icon_profile"></span></a>
                            </div>
                        </div>
<?php

                    }else{   

?>
                        <div class="col-lg-6">
                            <form  action="#" method="post">
                                <button type="submit" class="site-btn3" name="salir">Salir</button>
                            </form>
                        </div>
<?php

                    }  

?>
                                 
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
    <!-- Header End -->
    
    <form  action="#" method="post">
        <div class="wrap">
           <div class="search">
              <input type="text" class="searchTerm" name="titulo" placeholder="Buscar..">
              <button type="submit" class="searchButton" name="busqueda">
                <i class="fa fa-search"></i>
             </button>
           </div>
        </div>
    </form>

    <!-- Product Section Begin -->
    <section class="product spad">
        <div class="container">              
            <form action="#" method="post"> 
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-2">                    
                        <p><label class="opciones">Generos A-H</label></p>
                        <select name="genero1">
                            <option value="" label="vacio"></option>
                            <option value="Action">Acción</option>
                            <option value="Adventure">Aventura</option>
                            <option value="Martial_Arts">Artes marciales</option>
                            <option value="Sci-Fi">Ciencia ficción</option>
                            <option value="Comedy">Comedia</option>
                            <option value="Sports">Deportes</option>
                            <option value="Demons">Demonios</option>
                            <option value="Drama">Drama</option>
                            <option value="School">Escolares</option>
                            <option value="Fantasy">Fantasia</option>
                            <option value="Historical">Historicos</option>
                            <option value="Horror">Horror</option>                   
                        </select>
                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-2">                    
                        <p><label class="opciones">Generos I-Z</label></p>
                        <select name="genero2">
                            <option value="" label="vacio"></option>
                            <option value="Kids">Infantil</option>
                            <option value="Magic">Magia</option>
                            <option value="Mecha">Mecha</option>
                            <option value="Military">Militar</option>
                            <option value="Mystery">Misterio</option>
                            <option value="Music">Musical</option>
                            <option value="Slice_of_Life">Recuentos de la vida</option>
                            <option value="Romance">Romance</option>
                            <option value="Seinen">Seinen</option>
                            <option value="Shoujo">Shoujo</option>
                            <option value="Shounen">Shounen</option>
                            <option value="Supernatural">Sobrenatural</option>
                            <option value="Super_power">Super poderes</option>
                            <option value="Vampire">Vampiros</option>
                            
                        </select>
                    </div>


                    <div class="col-lg-3 col-md-3 col-sm-3">
                    
                        <p><label class="opciones">Orden</label></p>
                        <select name="orden">
                            <option value="tittle">Nombre A-Z</option>
                            <option value="episodes">Numero de capitulos</option>
                            <option value="id">Recientemente agregados</option>
                            <option value="score">Por calificación</option>
                        </select>
                    </div>   
                    <div class="row">
                        <div class="col-lg-1 col-md-1 col-sm-1">
                            <p><label class="opciones">Filtrar</label></p>     
                            <p><input type="submit" value="Buscar" name="buscar"><p>    
                        </div>
                    </div>  
                </div><br>
                <span id="rojo"> <?php echo $mensaje1;?></span>
            </form>
            
            <br><br>
            <div class="row">
                <div class="col-lg-8">
<?php   

    if ($cat == "ultimos") {

?>
                    <div class="trending__product">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <div class="section-title">
                                    <h4>Ultimos añadidos</h4>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">

<?php
  
        $sql1 = "SELECT id,tittle,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist ORDER BY id DESC LIMIT $start_from, $record_per_page";
        $result1 = mysqli_query ($conn, $sql1);

        if ($result1 == TRUE) {
            $number=0;
            while ($registro = mysqli_fetch_row($result1)) {
              $ep= round($registro[3]);
              $number++;

?>

                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="product__item">
                                    <div class="product__item__pic set-bg" data-setbg="<?php echo $registro[6]?>">
                                        <div class="ep"><?php echo $ep ?></div>
                                        <div class="comment"><i class="fa fa-comments"></i> <?php echo $registro[7] ?></div>
                                        <div class="view"><i class="fa fa-user"></i><?php echo $registro[4] ?></div>
                                    </div>
                                    <div class="product__item__text">
                                        <ul>
                                            <li><?php echo $registro[2]?></li>
                                        </ul>
                                        <h5><a href="anime-details.php?id=<?php echo $registro[0] ?>"><?php echo $registro[1] ?></a></h5>
                                    </div>
                                </div>
                            </div>
                            
<?php
            }

        }

?>
                        </div>
                    </div>
<?php    
            $sql10 = "SELECT id,tittle,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist ORDER BY id DESC ";
            $result10 = mysqli_query ($conn, $sql10);
            $total_records = mysqli_num_rows($result10);

            $total_pages = ceil($total_records/$record_per_page);
            $start_loop = $pagina;
            $diferencia = $total_pages - $pagina;

            if($diferencia <= 15){

             $start_loop = $total_pages - 6;

            }

            $end_loop = $start_loop + 5;

            if($pagina > 1){

             echo "<a class='pagina' href='categorias.php?categoria=ultimos&pagina=1'>Primera</a>";
             echo "<a class='pagina' href='categorias.php?categoria=ultimos&pagina=".($pagina - 1)."'><<</a>";

            }

            for($i=$start_loop; $i<=$end_loop; $i++){    

             echo "<a class='pagina' href='categorias.php?categoria=ultimos&pagina=".$i."'>".$i."</a>";
            }

            if($pagina <= $end_loop){

             echo "<a class='pagina' href='categorias.php?categoria=ultimos&pagina=".($pagina + 1)."'>>></a>";
             echo "<a class='pagina' href='categorias.php?categoria=ultimos&pagina=".$total_pages."'>Última</a>";

            }
    }

    if ($cat == "populares") {
?>
                    <div class="popular__product">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <div class="section-title">
                                    <h4>Animes mas populares</h4>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">

<?php
  
        $sql1 = "SELECT id,tittle,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist ORDER BY score DESC LIMIT $start_from, $record_per_page";
        $result1 = mysqli_query ($conn, $sql1);

        if ($result1 == TRUE) {
            $number=0;
            while ($registro = mysqli_fetch_row($result1)) {
              $ep= round($registro[3]);
              $number++;

?>

                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="product__item">
                                    <div class="product__item__pic set-bg" data-setbg="<?php echo $registro[6]?>">
                                        <div class="ep"><?php echo $ep ?></div>
                                        <div class="comment"><i class="fa fa-comments"></i> <?php echo $registro[7] ?></div>
                                        <div class="view"><i class="fa fa-user"></i><?php echo $registro[4] ?></div>
                                    </div>
                                    <div class="product__item__text">
                                        <ul>
                                            <li><?php echo $registro[2]?></li>
                                        </ul>
                                        <h5><a href="anime-details.php?id=<?php echo $registro[0] ?>"><?php echo $registro[1] ?></a></h5>
                                    </div>
                                </div>
                            </div>
                            
<?php
            }

        }

?>
                        </div>
                    </div>
<?php    
            $sql10 = "SELECT id,tittle,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist ORDER BY score DESC ";
            $result10 = mysqli_query ($conn, $sql10);
            $total_records = mysqli_num_rows($result10);

            $total_pages = ceil($total_records/$record_per_page);
            $start_loop = $pagina;
            $diferencia = $total_pages - $pagina;

            if($diferencia <= 15){

             $start_loop = $total_pages - 6;

            }

            $end_loop = $start_loop + 5;

            if($pagina > 1){

             echo "<a class='pagina' href='categorias.php?categoria=populares&pagina=1'>Primera</a>";
             echo "<a class='pagina' href='categorias.php?categoria=populares&pagina=".($pagina - 1)."'><<</a>";

            }

            for($i=$start_loop; $i<=$end_loop; $i++){    

             echo "<a class='pagina' href='categorias.php?categoria=populares&pagina=".$i."'>".$i."</a>";
            }

            if($pagina <= $end_loop){

             echo "<a class='pagina' href='categorias.php?categoria=populares&pagina=".($pagina + 1)."'>>></a>";
             echo "<a class='pagina' href='categorias.php?categoria=populares&pagina=".$total_pages."'>Última</a>";

            }
    }

    if ($cat == "miembros") {
?>
                    <div class="recent__product">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <div class="section-title">
                                    <h4>Animes con mas miembros</h4>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            

<?php
  
        $sql1 = "SELECT id,tittle,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist ORDER BY popularity ASC LIMIT $start_from, $record_per_page";
        $result1 = mysqli_query ($conn, $sql1);

        if ($result1 == TRUE) {
            $number=0;
            while ($registro = mysqli_fetch_row($result1)) {
              $ep= round($registro[3]);
              $number++;

?>

                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="product__item">
                                    <div class="product__item__pic set-bg" data-setbg="<?php echo $registro[6]?>">
                                        <div class="ep"><?php echo $ep ?></div>
                                        <div class="comment"><i class="fa fa-comments"></i> <?php echo $registro[7] ?></div>
                                        <div class="view"><i class="fa fa-user"></i><?php echo $registro[4] ?></div>
                                    </div>
                                    <div class="product__item__text">
                                        <ul>
                                            <li><?php echo $registro[2]?></li>
                                        </ul>
                                        <h5><a href="anime-details.php?id=<?php echo $registro[0] ?>"><?php echo $registro[1] ?></a></h5>
                                    </div>
                                </div>
                            </div>
                            
<?php
            }

        }

?>
                        </div>
                    </div>
<?php    
            $sql10 = "SELECT id,tittle,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist ORDER BY popularity ASC ";
            $result10 = mysqli_query ($conn, $sql10);
            $total_records = mysqli_num_rows($result10);

            $total_pages = ceil($total_records/$record_per_page);
            $start_loop = $pagina;
            $diferencia = $total_pages - $pagina;

            if($diferencia <= 15){

             $start_loop = $total_pages - 6;

            }

            $end_loop = $start_loop + 5;

            if($pagina > 1){

             echo "<a class='pagina' href='categorias.php?categoria=miembros&pagina=1'>Primera</a>";
             echo "<a class='pagina' href='categorias.php?categoria=miembros&pagina=".($pagina - 1)."'><<</a>";

            }

            for($i=$start_loop; $i<=$end_loop; $i++){    

             echo "<a class='pagina' href='categorias.php?categoria=miembros&pagina=".$i."'>".$i."</a>";
            }

            if($pagina <= $end_loop){

             echo "<a class='pagina' href='categorias.php?categoria=miembros&pagina=".($pagina + 1)."'>>></a>";
             echo "<a class='pagina' href='categorias.php?categoria=miembros&pagina=".$total_pages."'>Última</a>";

            }
    }

    if($cat != "ultimos" && $cat != "populares" && $cat != "miembros" && empty($filtro)){

?>
                    <div class="recent__product">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <div class="section-title">
                                    <h4>Animes de la categoría: <?php echo $cat ?></h4>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            

<?php

    
       
        

        $sql8 = "SELECT id,tittle,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist WHERE genre like '%$cat%' ORDER BY score DESC LIMIT $start_from, $record_per_page";
        $result8 = mysqli_query ($conn, $sql8);
        
        if ($result8 == TRUE) {
            $number=0;
            while ($registro = mysqli_fetch_row($result8)) {
              $ep= round($registro[3]);
              $number++;


?>

                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="product__item">
                                    <div class="product__item__pic set-bg" data-setbg="<?php echo $registro[6]?>">
                                        <div class="ep"><?php echo $ep ?></div>
                                        <div class="comment"><i class="fa fa-comments"></i> <?php echo $registro[7] ?></div>
                                        <div class="view"><i class="fa fa-user"></i><?php echo $registro[4] ?></div>
                                    </div>
                                    <div class="product__item__text">
                                        <ul>
                                            <li><?php echo $registro[2]?></li>
                                        </ul>
                                        <h5><a href="anime-details.php?id=<?php echo $registro[0] ?>"><?php echo $registro[1] ?></a></h5>
                                    </div>
                                </div>
                            </div>
                            
<?php
            }

        }

?>
                        </div>
                    </div>
<?php    
            $sql10 = "SELECT id,tittle,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist WHERE genre like '%$cat%' ORDER BY score DESC";
            $result10 = mysqli_query ($conn, $sql10);
            $total_records = mysqli_num_rows($result10);

            $total_pages = ceil($total_records/$record_per_page);
            $start_loop = $pagina;
            $diferencia = $total_pages - $pagina;

            if($diferencia <= 15){

             $start_loop = $total_pages - 6;

            }

            $end_loop = $start_loop + 5;

            if($pagina > 1){

             echo "<a class='pagina' href='categorias.php?categoria=".$cat."&pagina=1'>Primera</a>";
             echo "<a class='pagina' href='categorias.php?categoria=".$cat."&pagina=".($pagina - 1)."'><<</a>";

            }

            for($i=$start_loop; $i<=$end_loop; $i++){    

             echo "<a class='pagina' href='categorias.php?categoria=".$cat."&pagina=".$i."'>".$i."</a>";
            }

            if($pagina <= $end_loop){

             echo "<a class='pagina' href='categorias.php?categoria=".$cat."&pagina=".($pagina + 1)."'>>></a>";
             echo "<a class='pagina' href='categorias.php?categoria=".$cat."&pagina=".$total_pages."'>Última</a>";

            }
            
    }

    if($cat != "ultimos" && $cat != "populares" && $cat != "miembros" && !empty($filtro)){



?>
                    <div class="recent__product">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <div class="section-title">
                                    <h4>Animes de la categoría: <?php echo $cat ?></h4>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            

<?php

    
       if($filtro=='tittle'){
        
        $sql8 = "SELECT id,tittle,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist WHERE genre like '%$cat%' ORDER BY $filtro ASC LIMIT $start_from, $record_per_page";

        }

        if(($filtro=='episodes') || ($filtro=='id') || ($filtro=='score')){
        
        $sql8 = "SELECT id,tittle,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist WHERE genre like '%$cat%' ORDER BY $filtro DESC LIMIT $start_from, $record_per_page";

        } 

        if($filtro=='busqueda'){
        
        $sql8 = "SELECT id,tittle,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist  WHERE tittle like '%$cat%' ORDER BY tittle ASC LIMIT $start_from, $record_per_page";

        }


        $result8 = mysqli_query ($conn, $sql8);
        
        if ($result8 == TRUE) {
            $number=0;
            while ($registro = mysqli_fetch_row($result8)) {
              $ep= round($registro[3]);
              $number++;


?>

                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="product__item">
                                    <div class="product__item__pic set-bg" data-setbg="<?php echo $registro[6]?>">
                                        <div class="ep"><?php echo $ep ?></div>
                                        <div class="comment"><i class="fa fa-comments"></i> <?php echo $registro[7] ?></div>
                                        <div class="view"><i class="fa fa-user"></i><?php echo $registro[4] ?></div>
                                    </div>
                                    <div class="product__item__text">
                                        <ul>
                                            <li><?php echo $registro[2]?></li>
                                        </ul>
                                        <h5><a href="anime-details.php?id=<?php echo $registro[0] ?>"><?php echo $registro[1] ?></a></h5>
                                    </div>
                                </div>
                            </div>
                            
<?php
            }

        }

?>
                        </div>
                    </div>
<?php    


            
            if($filtro=='tittle'){

                $sql10 = "SELECT id,tittle,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist WHERE genre like '%$cat%' ORDER BY $filtro ASC";
            }

            if(($filtro=='episodes') || ($filtro=='id') || ($filtro=='score')){

                $sql10 = "SELECT id,tittle,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist WHERE genre like '%$cat%' ORDER BY $filtro DESC";
            }

            if($filtro=='busqueda'){

                $sql10 = "SELECT id,tittle,genre,episodes,members,score,img_url,NumeroComentarios FROM myanimelist WHERE tittle like '%$cat%' ORDER BY tittle ASC";
            }


            $result10 = mysqli_query ($conn, $sql10);
            $total_records = mysqli_num_rows($result10);

            $total_pages = ceil($total_records/$record_per_page);
            $start_loop = $pagina;
            $diferencia = $total_pages - $pagina;

            if($diferencia <= 15){

             $start_loop = $total_pages - 6;

            }

            $end_loop = $start_loop + 5;

            if($pagina > 1){

             echo "<a class='pagina' href='categorias.php?categoria=".$cat."&filtro=".$filtro."&pagina=1'>Primera</a>";
             echo "<a class='pagina' href='categorias.php?categoria=".$cat."&filtro=".$filtro."&pagina=".($pagina - 1)."'><<</a>";

            }

            for($i=$start_loop; $i<=$end_loop; $i++){

                if($i>=1){ 

                    echo "<a class='pagina' href='categorias.php?categoria=".$cat."&filtro=".$filtro."&pagina=".$i."'>".$i."</a>";
                }
            }


            if($pagina <= $end_loop){

             echo "<a class='pagina' href='categorias.php?categoria=".$cat."&filtro=".$filtro."&pagina=".($pagina + 1)."'>>></a>";
             echo "<a class='pagina' href='categorias.php?categoria=".$cat."&filtro=".$filtro."&pagina=".$total_pages."'>Última</a>";

            }
            
    }

?>



                   
                </div>
                <div class="col-lg-4 col-md-6 col-sm-8">
                    <div class="product__sidebar">
                        <div class="product__sidebar__view">
                            <div class="section-title">
                                <h5>Top Vistas</h5>
                            </div>
                            <form  action="#" method="post">
                                <ul class="filter__controls">
                                    <li data-filter=".day">Día</li>
                                    <li data-filter=".week">Semana</li>
                                    <li data-filter=".month">Mes</li>
                                    <li data-filter=".years">Año</li>
                                </ul>
                            </form>

<?php
  
        $sql1 = "SELECT tittle,id,episodes,Visionados,img_url FROM myanimelist ORDER BY Visionados DESC LIMIT 3";
        $result1 = mysqli_query ($conn, $sql1);

        $sql2 = "SELECT tittle,id,episodes,Visionados_mes,img_url FROM myanimelist ORDER BY Visionados_mes DESC LIMIT 3";
        $result2 = mysqli_query ($conn, $sql2);

        $sql3 = "SELECT tittle,id,episodes,Visionados_semana,img_url FROM myanimelist ORDER BY Visionados_semana DESC LIMIT 3";
        $result3 = mysqli_query ($conn, $sql3);

        $sql4 = "SELECT tittle,id,episodes,Visionados_dia,img_url FROM myanimelist ORDER BY Visionados_dia DESC LIMIT 3";
        $result4 = mysqli_query ($conn, $sql4);
 
         /*$sql1 = "SELECT tittle,id,Visionados_semana FROM myanimelist WHERE Visionados_dia='0' ";

        Esta sentencia de sql se ejecuta solo cuando los valores de las filas son 0, una vez que se han rellenado conjunto al codigo de abajo ya no es necesario y se puede mantener desactivado*/


        

            if ($result1 == TRUE) {

                while ($registro = mysqli_fetch_row($result1)) {

                /*$aleatorio=rand(1,$registro[2]);
                  $sql30 = " UPDATE myanimelist SET Visionados_dia='$aleatorio' WHERE  Visionados_dia='0' ";
                  $result30 = mysqli_query ($conn, $sql30);

                  Código para generar numeros aleatorios y que se añadan la fila de "visionados", simplemente para tener valores ficticios y poder trabajar con ellos */ 

?>

                                <div class="filter__gallery">
                                    <div class="product__sidebar__view__item set-bg mix years" 
                                    data-setbg="<?php echo $registro[4] ?>">
                                    <div class="ep"><?php echo $registro[2] ?></div>
                                    <div class="view"><i class="fa fa-eye"></i><?php echo $registro[3] ?></div>
                                    <h5><a href="anime-details.php?id=<?php echo $registro[1] ?>"><?php echo $registro[0] ?></a></h5>
                                </div>

<?php
                }

                
            }
        

    
                if ($result2 == TRUE) {

                    while ($registro = mysqli_fetch_row($result2)) {

?>

                                    <div class="filter__gallery">
                                        <div class="product__sidebar__view__item set-bg mix month" 
                                        data-setbg="<?php echo $registro[4] ?>">
                                        <div class="ep"><?php echo $registro[2] ?></div>
                                        <div class="view"><i class="fa fa-eye"></i><?php echo $registro[3] ?></div>
                                        <h5><a href="anime-details.php?id=<?php echo $registro[1] ?>"><?php echo $registro[0] ?></a></h5>
                                    </div></div>

<?php
                    }
                }
        

        
            if ($result3 == TRUE ) {

                while ($registro = mysqli_fetch_row($result3)) {

?>

                               <div class="filter__gallery">
                                    <div class="product__sidebar__view__item set-bg mix week" 
                                    data-setbg="<?php echo $registro[4] ?>">
                                    <div class="ep"><?php echo $registro[2] ?></div>
                                    <div class="view"><i class="fa fa-eye"></i><?php echo $registro[3] ?></div>
                                    <h5><a href="anime-details.php?id=<?php echo $registro[1] ?>"><?php echo $registro[0] ?></a></h5>
                                </div></div>

<?php
                }
            }
        

        
            if ($result4 == TRUE ) {

                while ($registro = mysqli_fetch_row($result4)) {

?>

                                <div class="filter__gallery">
                                    <div class="product__sidebar__view__item set-bg mix day" 
                                    data-setbg="<?php echo $registro[4] ?>">
                                    <div class="ep"><?php echo $registro[2] ?></div>
                                    <div class="view"><i class="fa fa-eye"></i><?php echo $registro[3] ?></div>
                                    <h5><a href="anime-details.php?id=<?php echo $registro[1] ?>"><?php echo $registro[0] ?></a></h5>
                                </div></div> 

<?php
                }
            }
        

?>

                             
            </div>
        </div>
    </div>
    <div class="product__sidebar__comment">
        <div class="section-title">
            <h5>Últimos comentarios</h5>
        </div>
<?php            
        
        $sql7 = "SELECT comentarios.id_comentario,comentarios.id,myanimelist.img_url,myanimelist.genre,myanimelist.tittle,myanimelist.Visionados FROM comentarios INNER JOIN myanimelist ON comentarios.id_comentario=myanimelist.id ORDER BY comentarios.id DESC LIMIT 4";
        $result7 = mysqli_query ($conn, $sql7);

        if ($result7 == TRUE) {

            while ($registro7 = mysqli_fetch_row($result7)) {
?>
                    <div class="product__sidebar__comment__item">
                        <div class="product__sidebar__comment__item__pic">
                            <img src="<?php echo $registro7[2] ?>" width="90" height="130" alt="">
                        </div>
                        <div class="product__sidebar__comment__item__text">
                            <ul>
                                <li><?php echo $registro7[3] ?></li>
                            </ul>
                            <h5><a href="anime-details.php?id=<?php echo $registro7[0] ?>"><?php echo $registro7[4] ?></a></h5>
                            <span><i class="fa fa-eye"></i><?php echo $registro7[5]?> Viewes</span>
                        </div>
                    </div>
                    
                
<?php 

            }
        }

?>
                </div>
</div>
</div>
</div>
</div>
</section>
<!-- Product Section End -->

<!-- Footer Section Begin -->
<footer class="footer">
    <div class="page-up">
        <a href="#" id="scrollToTopButton"><span class="arrow_carrot-up"></span></a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="footer__logo">
                    <a href="../index.php"><img src="../img/logo.png" alt=""></a>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="footer__nav">
                    <ul>
                        <li class="active"><a href="../index.php">Inicio</a></li>
                        <li><a href="categorias.php?categoria=<?php echo 'ultimos' ?>">Categories</a></li>
                        <li><a href="blog.php">Nuestro blog</a></li>
                        <li><a href="contacto.php">Contacto</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                    </ul>
                </div>
            </div>
            
          </div>
      </div>
  </footer>
  <!-- Footer Section End -->


  <!-- Search model Begin -->
  <div class="search-model">
    <div class="h-100 d-flex align-items-center justify-content-center">
        <div class="search-close-switch"><i class="icon_close"></i></div>
        <form class="search-model-form">
            <input type="text" id="search-input" placeholder="Search here.....">
        </form>
    </div>
</div>
<!-- Search model end -->

<!-- Js Plugins -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/player.js"></script>
<script src="../js/jquery.nice-select.min.js"></script>
<script src="../js/mixitup.min.js"></script>
<script src="../js/jquery.slicknav.js"></script>
<script src="../js/owl.carousel.min.js"></script>
<script src="../js/main.js"></script>


</body>

</html>