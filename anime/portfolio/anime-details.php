<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anime | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="../css/plyr.css" type="text/css">
    <link rel="stylesheet" href="../css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="../css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="../css/style.css" type="text/css">
</head>

<body>

<?php

session_start();
  
  $servername = "localhost";
  $username = "animeAdmin";
  $password = "animeAdmin";
  $dbname = "anime_db";

   $conn = mysqli_connect($servername, $username, $password,$dbname);

    $errores="";
    $aceptado="";
    $mensajes="";
    $id=$_GET['id'];

  if (isset($_POST['salir'])){

      session_destroy();
   
      header("location:login.php");
   }

   if (isset($_POST['busqueda'])){

        $titulo=$_POST['titulo'];
        
        header("location:categorias.php?categoria=$titulo&filtro=busqueda");

    }

    if (isset($_POST['delete'])) {

        $id1=$_POST["id"];

        $sql = " DELETE FROM comentarios WHERE id='$id1' ";
        $result = mysqli_query ($conn, $sql);

        header("location:anime-details.php?id=$id"); 

    }


   if (isset($_POST['postear'])) {

        header("location:anime-details.php?id=$id");             
    }


  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }
  
   

    $sql = " SELECT id,tittle,synopsis,genre,aired,episodes,members,Visionados,score,img_url,link,NumeroComentarios,popularity FROM myanimelist WHERE id='$id'";

    $result = mysqli_query ($conn, $sql);
        
    if(mysqli_num_rows($result) > 0){

        $registro = mysqli_fetch_row($result);
  
?>


    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="../index.php">
                            <img src="../img/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li><a href="../index.php">Inicio</a></li>
                                <li><a href="#">Categorias <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="categorias.php?categoria=<?php echo 'Action' ?>">Acción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Martial_Arts' ?>">Artes marciales</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Sci-Fi' ?>">Ciencia ficción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Comedy' ?>">Comedia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Drama' ?>">Drama</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'School' ?>">Escolares</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Fantasy' ?>">Fantasia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Horror' ?>">Horror</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Kids' ?>">Infantil</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Magic' ?>">Magia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mecha' ?>">Mecha</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mystery' ?>">Misterio</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Music' ?>">Musical</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Parody' ?>">Parodia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Slice_of_Life' ?>">Recuentos de la vida</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Romance' ?>">Romance</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Seinen' ?>">Seinen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Shounen' ?>">Shounen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Supernatural' ?>">Sobrenatural</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Super_power' ?>">Super poderes</a></li>
                                    </ul>
                                </li>
                                <li><a href="blog.php">Nuestro blog</a></li>
                                <li><a href="contacto.php">Contactanos</a></li>
<?php
  
                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Usuario") {
                                $user=$_SESSION["conectado"];

?>
                                <li><a href="perfil.php?user=<?php echo $user ?>">Perfil de usuario</a></li>
<?php

                            }

                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Admin") {

?>                              
                                <li><a href="admin_menu.php">Administración</a></li>
<?php

                            }

?>

                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    
<?php
  
                    if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

?>
                        <div class="col-lg-6">
                            <div class="header__right">
                                <a href="login.php"><span class="icon_profile"></span></a>
                            </div>
                        </div>
<?php

                    }else{   

?>
                        <div class="col-lg-6">
                            <form  action="#" method="post">
                                <button type="submit" class="site-btn3" name="salir">Salir</button>
                            </form>
                        </div>
<?php

                    }  

?>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
    <!-- Header End -->

    <form  action="#" method="post">
        <div class="wrap">
           <div class="search">
              <input type="text" class="searchTerm" name="titulo" placeholder="Buscar..">
              <button type="submit" class="searchButton" name="busqueda">
                <i class="fa fa-search"></i>
             </button>
           </div>
        </div>
    </form>

    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="../index.html"><i class="fa fa-home"></i> Home</a>
                        <a href="./categories.html">Categories</a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Anime Section Begin -->
    <section class="anime-details spad">
        <div class="container">
            <div class="anime__details__content">
                        <div class="anime__details__text">
                            <div class="anime__details__rating">
<?php
                            $sql10 = " SELECT score FROM myanimelist WHERE id='$id'";

                            $result10 = mysqli_query ($conn, $sql10);
        
                            if(mysqli_num_rows($result10) > 0){

                                $registro10 = mysqli_fetch_row($result10);


                                if($registro10[0]==0){

?>                               
                                    <div class="rating">
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                    </div>
<?php
                                }

                                if($registro10[0]>0 && $registro10[0]<2 ){

?>                               
                                    <div class="rating">
                                        <a href="#"><i class="fa fa-star-half-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                    </div>
<?php
                                }

                                if($registro10[0]==2){

?>                               
                                    <div class="rating">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                    </div>
<?php
                                }

                                if($registro10[0]>2 && $registro10[0]<4 ){

?>                               
                                    <div class="rating">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star-half-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                    </div>
<?php
                                }
                                if($registro10[0]==4){

?>                               
                                    <div class="rating">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                    </div>
<?php
                                }
                                 if($registro10[0]>4 && $registro10[0]<6 ){

?>                               
                                    <div class="rating">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star-half-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                    </div>
<?php
                                }
                                if($registro10[0]==6){

?>                               
                                    <div class="rating">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                    </div>
<?php
                                }
                                 if($registro10[0]>6 && $registro10[0]<8 ){

?>                               
                                    <div class="rating">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star-half-o"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                    </div>
<?php
                                }
                                if($registro10[0]==8){

?>                               
                                    <div class="rating">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star-o"></i></a>
                                    </div>
<?php
                                }
                                 if($registro10[0]>8 && $registro10[0]<10 ){

?>                               
                                    <div class="rating">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star-half-o"></i></a>
                                    </div>
<?php
                                }
                                if($registro10[0]==10){

?>                               
                                    <div class="rating">
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                        <a href="#"><i class="fa fa-star"></i></a>
                                    </div>
<?php
                                }
                            }
                          

?>                               
                                <span><?php echo $registro[12] ?> Votos</span>
                            </div><br><br><br>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="anime__details__pic set-bg" data-setbg="<?php echo $registro[9] ?>">
                            <div class="comment"><i class="fa fa-comments"></i><?php echo $registro[11] ?></div>
                            <div class="view"><i class="fa fa-eye"></i><?php echo $registro[7] ?></div>
                        </div>
                        <br>

 <?php
  
   if(isset($_SESSION["conectado"])) {

    $perfil=$_SESSION['conectado'];

    $sql15 = " SELECT Score,Progreso,Completo FROM listaanime WHERE idAnime='$id' AND NombreUsuario='$perfil'";

    $result15 = mysqli_query ($conn, $sql15);
                    
               


?>
                        
                        <div id="addtolist" class="addtolist-block js-anime-addtolist-block" style="">
                            <input type="hidden" id="myinfo_anime_id" value="16934">
                            <input type="hidden" id="myinfo_curstatus" value="">

                            <span style="color:#FFFFFF">* Your list is public by default.</span>

                       <button class="accordion">Añadir a la lista</button>
                        <div class="panel">
  
                        <form action="#"  method="post">
                            <table>
                              <tbody><tr>
                                <td class="spaceit">Estado:</td>
                                <td class="spaceit">
<?php
                            if(mysqli_num_rows($result15) > 0){

                                $registro15 = mysqli_fetch_row($result15);

?>
                                  <select name="status" id="myinfo_status" class="inputtext js-anime-status-dropdown"><option  selected="selected" value="<?php echo $registro15[2] ?>"><?php echo $registro15[2] ?></option><option value="Viendose">Viendo</option><option value="Terminado">Terminado</option><option value="Despues">Ver más tarde</option></select>
                                </td>
                              </tr>
                              <tr>
                                <td class="spaceit">Episodios Vistos:</td>
                                <td class="spaceit">
                                  <input type="text" name="episodios" size="3" class="inputtext" value="<?php echo $registro15[1] ?>"> / <span id="curEps"><?php echo $registro[5] ?></span></td>
                              </tr>
                              <tr>
                                <td class="spaceit">Puntuación:</td>
                                <td class="spaceit">
                                  <input type="text" name="puntuacion" id="myinfo_watchedeps" size="3" class="inputtext" value="<?php echo $registro15[0] ?>"> / 10
                                </td>
                              </tr>
<?php
                            }else{
        
?>
                                 <select name="status" id="myinfo_status" class="inputtext js-anime-status-dropdown"><option value="Viendose">Viendo</option><option value="Terminado">Terminado</option><option value="Despues">Ver más tarde</option></select>
                                </td>
                              </tr>
                              <tr>
                                <td class="spaceit">Episodios Vistos:</td>
                                <td class="spaceit">
                                  <input type="text" name="episodios" size="3" class="inputtext" value=""> / <span id="curEps"><?php echo $registro[5] ?></span></td>
                              </tr>
                              <tr>
                                <td class="spaceit">Puntuación:</td>
                                <td class="spaceit">
                                  <input type="text" name="puntuacion" id="myinfo_watchedeps" size="3" class="inputtext"> / 10
                                </td>
                              </tr>
<?php
                           }
?>
                              <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <button name="anadir" class="btn btn-primary">Añadir</button>
                                  
                                </td>
                              </tr>
                            </tbody>
                            </table>
                         </form>
                        </div>
                            <div id="myinfoDisplay" style="padding-left: 89px; margin-top: 3px;"></div>
                        </div>

<!-- Codigo Javascript para que se pueda guardar/desplegar el menú al presionar el botón -->

                        <script>
                        var acc = document.getElementsByClassName("accordion");
                        var i;

                        for (i = 0; i < acc.length; i++) {
                          acc[i].addEventListener("click", function() {
                            this.classList.toggle("active");
                            var panel = this.nextElementSibling;
                            if (panel.style.maxHeight) {
                              panel.style.maxHeight = null;
                            } else {
                              panel.style.maxHeight = panel.scrollHeight + "px";
                            } 
                          });
                        }
                        </script>
<?php

        if (isset($_POST['anadir'])){

            $valido=true;

            if(empty($_POST['episodios'])) {

                $ep="";

            }else{

                $ep=$_POST['episodios'];
            }

            if(empty($_POST['puntuacion'])) {

                $puntuacion="";

            }else{

                 $puntuacion=$_POST['puntuacion'];
            }


            $status=$_POST['status'];

            $f= date("Y").'-'.date("m").'-'.date("d");


            if($status=="Terminado"){ 

                if($_POST['episodios'] != $registro[5]){

                $errores= "No se puede marcar como terminado a no ser que se establezca el maximo de episodios";
                $valido=false;

                }else{
                    $valido=true;
                }
            }
        
            if($valido==true){

                $sql13 = " SELECT idAnime FROM listaanime WHERE idAnime='$id' AND NombreUsuario='$perfil'";

                $result13 = mysqli_query ($conn, $sql13);

                
                    
                if(mysqli_num_rows($result13) > 0){

                    $sql30 = " UPDATE listaanime SET Score='$puntuacion', Progreso='$ep',Completo='$status', UltimaActualizacion='$f' WHERE idAnime='$id' AND NombreUsuario='$perfil' ";
                    $result30 = mysqli_query ($conn, $sql30);


                    if ($result30 == FALSE) {
                        echo "Error en la ejecución de la consulta.<br />";
                    }else{
                        $aceptado="Se ha actualizado con exito ";
                    }

                }else{

                    if($status=="Viendose"){

                        if(($ep>$registro[5]) || ($ep < 0)){

                            $errores= "El numero de capitulos vistos, debe ser entre 0 y máximo de episodios";

                        }else{

                            $sql14= "INSERT INTO listaanime (id,idAnime,NombreUsuario,NombreAnime,Score,Progreso,Episodios,FechaInicio,UltimaActualizacion,Completo) VALUES (NULL,'$id','$perfil','$registro[1]','$puntuacion','$ep','$registro[5]','$f','$f','$status')";
                 
                             $result14 = mysqli_query ($conn, $sql14);

                              
                            if ($result14 == FALSE) {
                               
                                  echo "Error en la ejecución de la consulta 1.<br />";
                                  
                            }else{
                                  
                                
                                $aceptado="Se ha añadido con exito";
                             
                            }
                        }


                    }if($status=="Terminado"){

                        $sql18= "INSERT INTO listaanime (id,idAnime,NombreUsuario,NombreAnime,Score,Progreso,Episodios,FechaInicio,UltimaActualizacion,Completo) VALUES (NULL,'$id','$perfil','$registro[5]','$puntuacion','$ep','$f','$f','$status')";
             
                         $result18 = mysqli_query ($conn, $sql18);

                          
                        if ($result18 == FALSE) {
                           
                              echo "Error en la ejecución de la consulta 2.<br />";
                              
                        }else{
                              
                           $aceptado="Se ha añadido con exito";
                        }

                    }if($status=="Despues"){

                        $sql19= "INSERT INTO listaanime (id,idAnime,NombreUsuario,NombreAnime,Score,Progreso,Episodios,Completo) VALUES (NULL,'$id','$perfil','$registro[1]','$puntuacion','$ep','$registro[5]','$status')";
             
                         $result19 = mysqli_query ($conn, $sql19);

                          
                        if ($result19 == FALSE) {
                           
                              echo "Error en la ejecución de la consulta 3.<br />";
                              
                        }else{
                              
                           $aceptado="Se ha añadido con exito";
                        }

                    }

                }
            }
        }

    }
?>
                         <span id="rojo"> <?php echo $errores;?></span>
                        <span id="amarillo"> <?php echo $aceptado;?></span>
                    </div>
                    <div class="col-lg-9">


                            <div class="anime__details__title">
                                <h3><?php echo $registro[1] ?></h3>
                                
                            </div>
                           
                            <p><?php echo $registro[2] ?></p>
                            <div class="anime__details__widget">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <ul>
                                            <li><span>Tipo:</span> TV Series</li>
                                            <li><span>Emisión:</span><?php echo $registro[4] ?></li>
                                            <li><span>Genero:</span><?php echo $registro[3] ?></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <ul>
                                            <li><span>Puntuación:</span><?php echo $registro[8] ?></li>
                                            <li><span>Miembros:</span><?php echo $registro[6] ?></li>
                                            <li><span>Calidad:</span> HD</li>
                                            <li><span>Visionado:</span><?php echo $registro[7] ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="anime__details__btn">
                                <a href="#" class="follow-btn"><i class="fa fa-heart-o"></i> Follow</a>
                                <a href="<?php echo $registro[10] ?>" class="watch-btn"><span>Ver ficha completa</span> <i
                                    class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-8">
                        <div class="anime__details__review">
                            <div class="section-title">
                                <h5>Comentarios</h5>
                            </div>

<?php
                $sql30 = " SELECT comentarios.Autor,comentarios.Contenido,login.Foto,comentarios.Fecha,login.Rol, comentarios.id FROM comentarios INNER JOIN login ON comentarios.Autor=login.NombreUsuario WHERE id_comentario='$id' ORDER BY comentarios.id ASC ";

                $result30 = mysqli_query ($conn, $sql30);
                
                $autor="";

                    if ($result30 == TRUE) {

                        while ($registro = mysqli_fetch_row($result30)) {  

                            if ($registro[4]=='Admin') {

                                $autor="autor1";
                            }else{

                                $autor="autor2";
                            }                                     

?>                          
                            <form action="#"  method="post"> 
                                <input name="id" type="hidden" value="<?php echo $registro[5] ?>">        
                                <div class="anime__review__item">
                                    <div class="anime__review__item__pic">
                                        <a href="perfil.php?user=<?php echo $registro[0] ?>"><img src="<?php echo $registro[2] ?>" alt="profile"></a>
                                    </div>
                                    <div class="anime__review__item__text">
                                        <a href="perfil.php?user=<?php echo $registro[0] ?>" class="<?php echo $autor ?>"><?php echo $registro[0] ?> - <span><?php echo $registro[3] ?></span></a>
                                        <p><?php echo $registro[1] ?></p>
<?php                                    
                                        if (isset($_SESSION["conectado"])){

                                            if ($_SESSION["conectado"]==$registro[0]){
?> 
                                                <button type="submit" class="btn btn-danger float-right" name="delete" >Borrar</button>
<?php                                    
                                            }
                                        }
?> 
                                    
                                    </div>
                                </div>
                            </form>

<?php
  
                        }
                    }

?>                                

                        </div>
<?php
  
                    if(isset($_SESSION["conectado"])) {
?>  
                        <div class="anime__details__form">
                            <div class="section-title">
                                <h5>Escribe tu comentario</h5>
                            </div>
                            <form action="#"  method="post">
                                <textarea placeholder="¿Qué opinas del contenido?" name="mensaje"></textarea>
                                <button type="submit" name="postear"><i class="fa fa-location-arrow"></i> Postear</button>
                            </form>
                            <br><span id="amarillo"> <?php echo $mensajes;?></span>
                        </div>
<?php
  
                        
                    }
                    if (isset($_POST['postear'])) {

                        $msg=$_POST["mensaje"];
                        $user=$_SESSION['conectado'];


                        $f= date("Y").'-'.date("m").'-'.date("d");


                        $sql130= "INSERT INTO comentarios (id,Autor,Contenido,Tipo,id_comentario,Fecha) VALUES (NULL,'$user','$msg','Anime','$id','$f')";
         
                         $result130 = mysqli_query ($conn, $sql130);

                          
                        if ($result130 == FALSE) {
                           
                              echo "Error en la ejecución de la consulta.<br />";
                              
                        }


                        $sql150 = " SELECT NumeroComentarios FROM myanimelist WHERE id='$id'";

                        $result150 = mysqli_query ($conn, $sql150);


                        if(mysqli_num_rows($result150) > 0){

                            $registro150 = mysqli_fetch_row($result150);

                            $coment=$registro150[0]+1;

                            $sql140= " UPDATE myanimelist SET NumeroComentarios='$coment' WHERE id='$id'";
             
                             $result140 = mysqli_query ($conn, $sql140);

                              
                            if ($result140 == FALSE) {
                               
                                  echo "Error en la ejecución de la consulta.<br />";
                                  
                            }

                        }


                    }


?>  
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="anime__details__sidebar">
                            <div class="section-title">
                                <h5>Quizas te guste...</h5>
                            </div>
<?php
  
        $sql1 = "SELECT tittle,id,episodes,Visionados,img_url FROM myanimelist ORDER BY score DESC LIMIT 4";
        $result1 = mysqli_query ($conn, $sql1);


        if ($result1 == TRUE) {

            while ($registro = mysqli_fetch_row($result1)) {

?>
                            <div class="filter__gallery">
                                <div class="product__sidebar__view__item set-bg mix day years"
                                data-setbg="<?php echo $registro[4] ?>">
                                <div class="ep"><?php echo $registro[2] ?></div>
                                <div class="view"><i class="fa fa-eye"></i><?php echo $registro[3] ?></div>
                                <h5><a href="anime-details.php?id=<?php echo $registro[1] ?>"><?php echo $registro[0] ?></a></h5>
                            </div>                          
<?php
            }
        }

?>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </section>
        <!-- Anime Section End -->

        <!-- Footer Section Begin -->
        <footer class="footer">
            <div class="page-up">
                <a href="#" id="scrollToTopButton"><span class="arrow_carrot-up"></span></a>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="footer__logo">
                            <a href="../index.php"><img src="../img/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="footer__nav">
                            <ul>
                                <li class="active"><a href="../index.php">Inicio</a></li>
                                <li><a href="categorias.php?categoria=<?php echo 'ultimos' ?>">Categories</a></li>
                                <li><a href="blog.php">Nuestro blog</a></li>
                                <li><a href="contacto.php">Contacto</a></li>
                                <li><a href="faq.php">FAQ</a></li>
                            </ul>
                        </div>
                    </div>
                    
                  </div>
              </div>
          </footer>
          <!-- Footer Section End -->

          <!-- Search model Begin -->
          <div class="search-model">
            <div class="h-100 d-flex align-items-center justify-content-center">
                <div class="search-close-switch"><i class="icon_close"></i></div>
                <form class="search-model-form">
                    <input type="text" id="search-input" placeholder="Search here.....">
                </form>
            </div>
        </div>
        <!-- Search model end -->

        <!-- Js Plugins -->
        <script src="../js/jquery-3.3.1.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/player.js"></script>
        <script src="../js/jquery.nice-select.min.js"></script>
        <script src="../js/mixitup.min.js"></script>
        <script src="../js/jquery.slicknav.js"></script>
        <script src="../js/owl.carousel.min.js"></script>
        <script src="../js/main.js"></script>



<?php  
       
    }
?>

    </body>

    </html>