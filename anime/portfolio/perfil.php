<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anime | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="../css/plyr.css" type="text/css">
    <link rel="stylesheet" href="../css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="../css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="../css/style.css" type="text/css">
</head>

<body>

<?php
  session_start();

    

    function test_input($data) {
          $data = trim($data); 
          $data = stripslashes($data); 
         return $data;
    }

    
    $perfil="";
    

    if (isset($_SESSION["conectado"])){

         if (isset($_GET['user']) && $_GET['user']==$_SESSION["conectado"]){

            $perfil=$_SESSION["conectado"];
        }
    }

    if (!isset($_SESSION["conectado"]) && isset($_GET['user'])){

            $perfil=$_GET['user'];
        
    }

    if (isset($_SESSION["conectado"]) && $_GET['user']!=$_SESSION["conectado"]){
   
        $perfil=$_GET['user'];
    }

    if (!isset($_SESSION["conectado"]) && !isset($_GET['user'])){

            header("location:../index.php");
        
    }

    if (isset($_SESSION["conectado"]) && !isset($_GET['user'])){

            $perfil=$_SESSION["conectado"];
            header("location:perfil.php?user=$perfil");
        
    }

    $servername = "localhost";
    $username = "animeAdmin";
    $password = "animeAdmin";
    $dbname = "anime_db";

    $conn = mysqli_connect($servername, $username, $password,$dbname);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    if (isset($_POST['salir'])){

      session_destroy();
   

      header("location:login.php");
   }

   if (isset($_POST['borra'])){

      header("location:perfil.php?user=$perfil");
   }

   if (isset($_POST['datos'])){

      header("location:perfil.php?user=$perfil");
   }

    $passError=$usuarioError="";
    $pass=  "";
    $contra= false;

    if (isset($_POST['contrasena'])){

           $contra1=$_POST["rpass"];
           $contra2=$_POST["rpass1"];

        if(!empty($contra1) && !empty($contra2)){
            if ($contra1!=$contra2) {
                $passError = "* Las contraseñas no coinciden <br>";
            }else{
                $pass = test_input($contra1);
                $contra = true;
            }
        }else{
            $passError = "* Introduce la contraseña <br>";
        }

    }



    if($contra==true){
        if (isset($_POST['contrasena'])){

            
            $oldpass=$_POST["oldpass"];
            $newpass=$_POST["rpass"];

            $encript= hash_hmac('sha512', $oldpass, 'secret');
            $encript1= hash_hmac('sha512', $newpass, 'secret');

            $comprobacion = "SELECT Pass FROM login WHERE NombreUsuario='$perfil'";
            $result1 = mysqli_query ($conn, $comprobacion);

            $registro = mysqli_fetch_row($result1);
            
            if($registro[0]!=$encript){
            
                $usuarioError = "* La contraseña antigua no coincide con la almacenada en nuestra base de datos, vuelve a introducirla <br>";                
            
            }else{

                $sql9 = " UPDATE login SET Pass='$encript1' WHERE NombreUsuario='$perfil' ";
                $result1 = mysqli_query ($conn, $sql9);

                if ($result1 == FALSE) {
                    echo "Error en la ejecución de la consulta.<br />";

                }

            }
        }
    }


    if (isset($_POST['datos'])){

            
        $name=$_POST["nombre"];
        $apell=$_POST["apellido"];
        $email=$_POST["email"];           
        $telefono=$_POST["telefono"];
        $twitter=$_POST["twitter"];
        $instagram=$_POST["instagram"];
        $foto=$_POST["foto"];
        $bio=$_POST["bio"];
        

        $sql = " UPDATE login SET mail='$email', Nombre='$name',  Apellidos='$apell',  Tlf='$telefono',  Twitter='$twitter',  Instagram='$instagram',  Bio='$bio', Foto='$foto' WHERE NombreUsuario='$perfil' ";
            $result = mysqli_query ($conn, $sql);

            if ($result == FALSE) {
                echo "Error en la ejecución de la consulta.<br />";                
             }
        
    }

?>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="../index.php">
                            <img src="../img/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li><a href="../index.php">Inicio</a></li>
                                <li><a href="#">Categorias <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="categorias.php?categoria=<?php echo 'Action' ?>">Acción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Martial_Arts' ?>">Artes marciales</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Sci-Fi' ?>">Ciencia ficción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Comedy' ?>">Comedia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Drama' ?>">Drama</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'School' ?>">Escolares</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Fantasy' ?>">Fantasia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Horror' ?>">Horror</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Kids' ?>">Infantil</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Magic' ?>">Magia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mecha' ?>">Mecha</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mystery' ?>">Misterio</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Music' ?>">Musical</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Parody' ?>">Parodia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Slice_of_Life' ?>">Recuentos de la vida</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Romance' ?>">Romance</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Seinen' ?>">Seinen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Shounen' ?>">Shounen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Supernatural' ?>">Sobrenatural</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Super_power' ?>">Super poderes</a></li>
                                    </ul>
                                </li>
                                <li><a href="blog.php">Nuestro blog</a></li>
                                <li><a href="contacto.php">Contactanos</a></li>
<?php
  
                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Usuario") {

?>
                                <li class="active"><a href="perfil.php?user=<?php echo $user ?>">Perfil de usuario</a></li>
<?php

                            }

                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Admin") {

?>                              
                                <li><a href="admin_menu.php">Administración</a></li>
<?php

                            }

?>

                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    
<?php
  
                    if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

?>
                        <div class="col-lg-6">
                            <div class="header__right">
                                <a href="login.php"><span class="icon_profile"></span></a>
                            </div>
                        </div>
<?php

                    }else{   

?>
                        <div class="col-lg-6">
                            <form  action="" method="post">
                                <button type="submit" class="site-btn3" name="salir">Salir</button>
                            </form>
                        </div>
<?php

                    }  

?>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
    <!-- Header End -->

<?php

    if(isset($_SESSION["conectado"]) && ($_SESSION["conectado"]==$perfil )){

        $perfil=$_SESSION["conectado"];
  
        $sql1 = "SELECT NombreUsuario,Mail,Foto,Nombre,Apellidos,Tlf,Twitter,Instagram,Bio FROM login WHERE NombreUsuario='$perfil' ";
        $result1 = mysqli_query ($conn, $sql1);
   
        if(mysqli_num_rows($result1) > 0){

            $registro = mysqli_fetch_row($result1);
  
?>


    <section class="py-5 my-5">
        <div class="container">
            <h1 style="color:#FFFFFF">Perfil de usuario</h1><br>
            <div class="bg-white shadow rounded-lg d-block d-sm-flex">
                <div class="profile-tab-nav border-right">
                    <div class="p-4">
                        <div class="img-circle text-center mb-3">
                            <img src="<?php echo $registro[2] ?>" alt="Image" class="shadow">
                        </div>
                        <h4 class="text-center"><?php echo $registro[0] ?></h4>
                    </div>
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="account-tab" data-toggle="pill" href="#account" role="tab" aria-controls="account" aria-selected="true">
                            <i class="fa fa-home text-center mr-1"></i> 
                            Cuenta
                        </a>
                        <a class="nav-link" id="password-tab" data-toggle="pill" href="#password" role="tab" aria-controls="password" aria-selected="false">
                            <i class="fa fa-key text-center mr-1"></i> 
                            Contraseña
                        </a>
                        <a class="nav-link" id="security-tab" data-toggle="pill" href="#security" role="tab" aria-controls="security" aria-selected="false">
                            <i class="fa fa-bars" aria-hidden="true"></i>

                            Lista completa de Anime
                        </a>
                        <a class="nav-link" id="application-tab" data-toggle="pill" href="#application" role="tab" aria-controls="application" aria-selected="false">
                            <i class="fa fa-battery-full" aria-hidden="true"></i>
                            Animes terminados
                        </a>
                        <a class="nav-link" id="notification-tab" data-toggle="pill" href="#notification" role="tab" aria-controls="notification" aria-selected="false">
                            <i class="fa fa-battery-half" aria-hidden="true"></i> 
                            Animes empezados
                        </a>
                        <a class="nav-link" id="notification-tab" data-toggle="pill" href="#despues" role="tab" aria-controls="despues" aria-selected="false">
                            <i class="fa fa-battery-empty" aria-hidden="true"></i> 
                            Ver mas tarde
                        </a>
                        <a class="nav-link" id="notification-tab" data-toggle="pill" href="#buzon" role="tab" aria-controls="buzon" aria-selected="false">
                            <i class="fa fa-share" aria-hidden="true"></i> 
                            Buzón de mensajes
                        </a>
                        <a class="nav-link" id="notification-tab" data-toggle="pill" href="#redactar" role="tab" aria-controls="redactar" aria-selected="false">
                            <i class="fa fa-reply" aria-hidden="true"></i> 
                            Enviar mensaje
                        </a><br>
                        <form>
                            <button type="submit" class="site-btn" name="salir">Cerrar sesisón</button>
                        </form>
                    </div>
                </div>
                <div class="tab-content p-4 p-md-5" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="account" role="tabpanel" aria-labelledby="account-tab">
                        <h3 class="mb-4">Ajustes de cuenta</h3>
                            <form action=""  method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nombre</label>&nbsp;<i class="fa fa-address-book"></i>
                                            <input value="<?php echo $registro[3] ?>" type="text" class="form-control" name="nombre">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Apellidos</label>&nbsp;<i class="fa fa-address-book-o"></i>
                                            <input value="<?php echo $registro[4] ?>" type="text" class="form-control" name="apellido">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email</label>&nbsp;<i class="fa fa-envelope-open" aria-hidden="true"></i>
                                            <input value="<?php echo $registro[1] ?>" type="text" class="form-control" name="email">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Teléfono</label>&nbsp;<i class="fa fa-phone"></i>
                                            <input value="<?php echo $registro[5] ?>" type="text" class="form-control" name="telefono">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Twitter</label>&nbsp;<i class="fa fa-twitter"></i>
                                            <input value="<?php echo $registro[6] ?>" type="text" class="form-control" name="twitter">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Instagram</label>&nbsp;<i class="fa fa-instagram"></i>
                                            <input value="<?php echo $registro[7] ?>" type="text" class="form-control" name="instagram">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>URL de la imagen</label>&nbsp;<i class="fa fa-file-image-o"></i>
                                            <input value="<?php echo $registro[2] ?>" type="text" class="form-control" name="foto">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Bio</label>&nbsp;<i class="fa fa-book"></i>
                                            <textarea class="form-control" rows="4" name="bio"><?php echo $registro[8]?></textarea>
                                        </div>
                                    </div>

                            
                                </div>
                                <button class="site-btn1" type="submit" name="datos"> Actualizar</button>
                            </form>
                        <div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="password-tab">
                        <h3 class="mb-4">Ajustes de contraseña</h3>
                        
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contraseña Antigua</label>&nbsp;<i class="fa fa-lock"></i>
                                        <input name="oldpass" id="oldpass" type="password" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nueva Contraseña</label>&nbsp;<i class="fa fa-lock"></i>
                                        <input name="rpass" id="rpass" type="password" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Repetir contraseña</label>&nbsp;<i class="fa fa-lock"></i>
                                        <input name="rpass1" id="rpass1" type="password" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div>
                                
                                <button class="site-btn1" type="submit" name="contrasena" onClick="recp1('<?=$perfil?>')"> Actualizar</button>
                            </div>

                            <div id='myStyle1'></div>
                               
                    </div>
                    <div class="tab-pane fade" id="security" role="tabpanel" aria-labelledby="security-tab">
                        <h3 class="mb-4">Lista de anime</h3>
                        <div class="row">


                            <section class="margen">
                            <div id="div1">    
                                <div class="tbl-header">
                                    <table class="perfil" cellpadding="0" cellspacing="0" border="0">
                                      <thead>
                                        <tr>
                                          <th>Anime</th>
                                          <th>Puntuación</th>
                                          <th>Progreso</th>
                                          <th>Episodios</th>
                                          <th>Fecha inicio</th>
                                          <th>Ultima actualización</th>
                                          <th>Estado</th>
                                        </tr>
                                      </thead>
                                    
                                  

                                        
                                    
                                            <tbody>
                                      
<?php

                    $sql100 = "SELECT NombreUsuario ,NombreAnime,Score,Progreso,Episodios,FechaInicio,UltimaActualizacion,Completo FROM listaanime WHERE NombreUsuario='$perfil'";
        
                    $result100 = mysqli_query ($conn, $sql100);
        
                    $columna=0;
                    if ($result100 == TRUE) {

                        while ($registro11 = mysqli_fetch_row($result100)) {
                            
                            if($columna%2==0){

                                $color="color1";

                            }else{

                                $color="color2";
                            } 
?>
                                                <tr>
                                                  <td class="<?php echo $color ?>"><?php echo $registro11[1] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro11[2] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro11[3] ?>/<?php echo $registro11[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro11[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro11[5] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro11[6] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro11[7] ?></td>
                                                </tr>                                                                               
<?php
                            $columna++;
                        }
  
                    }     
  
?>
                                            </tbody>
                                        
                                    </table>
                                  </div>
                            </div>
                            </section>
                        </div>
                       
                    </div>
                    <div class="tab-pane fade" id="application" role="tabpanel" aria-labelledby="application-tab">
                        <h3 class="mb-4">Lista de animes terminados</h3>
                          <div class="row">


                            <section class="margen">
                            <div id="div1">    
                                <div class="tbl-header">
                                    <table class="perfil" cellpadding="0" cellspacing="0" border="0">
                                      <thead>
                                        <tr>
                                          <th>Anime</th>
                                          <th>Puntuación</th>
                                          <th>Progreso</th>
                                          <th>Episodios</th>
                                          <th>Fecha inicio</th>
                                          <th>Ultima actualización</th>
                                          <th>Estado</th>
                                        </tr>
                                      </thead>
                                    
                                  

                                        
                                    
                                            <tbody>
                                      
<?php

                    $sql110 = "SELECT NombreUsuario ,NombreAnime,Score,Progreso,Episodios,FechaInicio,UltimaActualizacion,Completo FROM listaanime WHERE NombreUsuario='$perfil' AND Completo='Terminado'";
        
                    $result110 = mysqli_query ($conn, $sql110);
        
                    $columna=0;
                    if ($result110 == TRUE) {

                        while ($registro110 = mysqli_fetch_row($result110)) {
                            
                            if($columna%2==0){

                                $color="color1";

                            }else{

                                $color="color2";
                            } 
?>
                                                <tr>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[1] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[2] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[3] ?>/<?php echo $registro110[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[5] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[6] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[7] ?></td>
                                                </tr>                                                                               
<?php
                            $columna++;
                        }
  
                    }     
  
?>
                                            </tbody>
                                        
                                    </table>
                                  </div>
                            </div>
                            </section>
                        </div>
                       
                    </div>
                    <div class="tab-pane fade" id="notification" role="tabpanel" aria-labelledby="notification-tab">
                        <h3 class="mb-4">Lista de animes empezados</h3>
                          <div class="row">


                            <section class="margen">
                            <div id="div1">    
                                <div class="tbl-header">
                                    <table class="perfil" cellpadding="0" cellspacing="0" border="0">
                                      <thead>
                                        <tr>
                                          <th>Anime</th>
                                          <th>Puntuación</th>
                                          <th>Progreso</th>
                                          <th>Episodios</th>
                                          <th>Fecha inicio</th>
                                          <th>Ultima actualización</th>
                                          <th>Estado</th>
                                        </tr>
                                      </thead>
                                    
                                  

                                        
                                    
                                            <tbody>
                                      
<?php

                    $sql110 = "SELECT NombreUsuario ,NombreAnime,Score,Progreso,Episodios,FechaInicio,UltimaActualizacion,Completo FROM listaanime WHERE NombreUsuario='$perfil' AND Completo='Viendose'";
        
                    $result110 = mysqli_query ($conn, $sql110);
        
                    $columna=0;
                    if ($result110 == TRUE) {

                        while ($registro110 = mysqli_fetch_row($result110)) {
                            
                            if($columna%2==0){

                                $color="color1";

                            }else{

                                $color="color2";
                            } 
?>
                                                <tr>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[1] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[2] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[3] ?>/<?php echo $registro110[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[5] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[6] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[7] ?></td>
                                                </tr>                                                                               
<?php
                            $columna++;
                        }
  
                    }     
  
?>
                                            </tbody>
                                        
                                    </table>
                                  </div>
                            </div>
                            </section>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="despues" role="despues" aria-labelledby="despues-tab">
                        <h3 class="mb-4">Ver más tarde</h3>
                          <div class="row">


                            <section class="margen">
                            <div id="div1">    
                                <div class="tbl-header">
                                    <table class="perfil" cellpadding="0" cellspacing="0" border="0">
                                      <thead>
                                        <tr>
                                          <th>Anime</th>
                                          <th>Puntuación</th>
                                          <th>Progreso</th>
                                          <th>Episodios</th>
                                          <th>Fecha inicio</th>
                                          <th>Ultima actualización</th>
                                          <th>Estado</th>
                                        </tr>
                                      </thead>
                                    
                                  

                                        
                                    
                                            <tbody>
                                      
<?php

                    $sql110 = "SELECT NombreUsuario ,NombreAnime,Score,Progreso,Episodios,FechaInicio,UltimaActualizacion,Completo FROM listaanime WHERE NombreUsuario='$perfil' AND Completo='Despues'";
        
                    $result110 = mysqli_query ($conn, $sql110);
        
                    $columna=0;
                    if ($result110 == TRUE) {

                        while ($registro110 = mysqli_fetch_row($result110)) {
                            
                            if($columna%2==0){

                                $color="color1";

                            }else{

                                $color="color2";
                            } 
?>
                                                <tr>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[1] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[2] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[3] ?>/<?php echo $registro110[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[5] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[6] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[7] ?></td>
                                                </tr>                                                                               
<?php
                            $columna++;
                        }  
                    }       
?>                                            </tbody>
                                    </table>
                                  </div>
                            </div>
                            </section>
                        </div>
                       
                    </div>

                    <div class="tab-pane fade" id="buzon" role="buzon" aria-labelledby="buzon-tab">
                        <h3 class="mb-4">Buzón de mensajes</h3>


                        <section >
                            <div class="container">

                                <div id='myStyle'></div>
        
                            </div>
                        </section >

                  
<?php
        

          $sql1 = "SELECT mensajes.id, mensajes.Emisor, mensajes.Asunto, mensajes.Fecha, mensajes.Leido FROM mensajes INNER JOIN login ON mensajes.Receptor=login.Mail WHERE login.NombreUsuario='$perfil'";

          

          $result = mysqli_query ($conn, $sql1);

            
            if ($result == FALSE) {
                echo "Error en la ejecución de la consulta.<br />";
            }else {
?>

                        <section >
                            <div class="container1" style="color:black">
                                <form  action="#" method="post">
<?php
                        echo "<div class='tabla'>";
                            echo "<table style='width:100%'>";
                                echo "<tr>";
                                    echo "<th>".""." <br>";
                                    echo "<th>" ."Asunto "."</th>";
                                    echo "<th>" ."Mensaje de "."</th>";
                                    echo "<th>" ." Leído  "."</th>";
                                    echo "<th>" ." Fecha  "."</th>";
                                echo "</tr>";

                while ($registro = mysqli_fetch_row($result)) {
                
                                echo "<tr>";
?>
                                    <td><input type="checkbox" name="checkList[]" value="<?php echo $registro[0] ?>"></td>
<?php
                                    echo "<td><a href='#' class='enlace' onClick='recp($registro[0])' > " .$registro[2]."</td>";
                                    echo "<td>" .$registro[1]."</td>";
                                    echo "<td>" .$registro[4]."</td>";
                                    echo "<td>" .$registro[3]."</td>";
                                echo "</tr>";

                }
                        echo "</table>"."<br>";
                    echo "</div>";
?>
                                    <br><br>

                                    <div align="center">
                                        <button class="site-btn1" type="submit" name="borra"> Borrar mensaje</button>
                                    </div>
                                </form>
                            </div>
                        </section >

<?php                   
           }
        
        
?>
        
<?php 

                if (isset($_POST['borra'])){
                    if (isset($_POST['checkList'])) {
                        $cuenta=count($_POST['checkList']);

                        if ($cuenta==1){

                            foreach ($_POST['checkList'] as $selected1) {
                                $sql3 = "DELETE from mensajes where id='$selected1'";
                                $result2 = mysqli_query ($conn, $sql3);
                 
                                if ($result2 == FALSE) {
                                    echo "Error en la ejecución de la consulta.<br />";
                                }
                            }
                        }else{
                            echo "<br><br>";
                            echo "<h3 class='titulo'>Escoge solo un mensaje para borrar.</h3><br>";

                        }
                    }else{
                        echo "<br><br>";
                        echo "<h3 class='titulo'>Debes escoger almenos un mensaje para borrar.</h3><br>";

                    }
                }
?>

                    </div>

                    <div class="tab-pane fade" id="redactar" role="redactar" aria-labelledby="redactar-tab">
                        <h3 class="mb-4">Enviar mensaje</h3>

                        <div id="texto1">
                            <section>

                                <div class="container">

                                    <b>Asunto</b> <input class="campo" name="asunto" id="asunt" type="text" placeholder="Asunto del mensaje" align="center"><br><br>
                                       
                                    <b>Destinatario</b> <input class="campo" name="dest" id="dest" type="text" placeholder="contacto@correo.com"><br><br>
                                       
                                    <b>Mensaje:</b> <textarea name="comment" rows="5" id="com" cols="40"></textarea>
                                    <br><br>  
                                    <button class="site-btn1" type="submit" onClick="recp2('<?=$perfil?>')"> Enviar mensaje</button>
                                </div>
                            </section>
                        </div>

                        <div id='myStyle2'></div>
                  
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php  
       
        }
    }else{

        $sql1 = "SELECT NombreUsuario,Mail,Foto,Nombre,Apellidos,Tlf,Twitter,Instagram,Bio,TipoLista FROM login WHERE NombreUsuario='$perfil' ";
        $result1 = mysqli_query ($conn, $sql1);
   
        if(mysqli_num_rows($result1) > 0){

            $registro = mysqli_fetch_row($result1);

            $visible=$registro[9];
            
  
?>


    <section class="py-5 my-5">
        <div class="container">
            <h1 style="color:#FFFFFF">Perfil de usuario</h1><br>
            <div class="bg-white shadow rounded-lg d-block d-sm-flex">
                <div class="profile-tab-nav border-right">
                    <div class="p-4">
                        <div class="img-circle text-center mb-3">
                            <img src="<?php echo $registro[2] ?>" alt="Image" class="shadow">
                        </div>
                        <h4 class="text-center"><?php echo $registro[0] ?></h4>
                    </div>
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="account-tab" data-toggle="pill" href="#account" role="tab" aria-controls="account" aria-selected="true">
                            <i class="fa fa-home text-center mr-1"></i> 
                            Cuenta
                        </a>
                       
                        <a class="nav-link" id="security-tab" data-toggle="pill" href="#security" role="tab" aria-controls="security" aria-selected="false">
                            <i class="fa fa-bars" aria-hidden="true"></i>

                            Lista completa de Anime
                        </a>
                        <a class="nav-link" id="application-tab" data-toggle="pill" href="#application" role="tab" aria-controls="application" aria-selected="false">
                            <i class="fa fa-battery-full" aria-hidden="true"></i>
                            Animes terminados
                        </a>
                        <a class="nav-link" id="notification-tab" data-toggle="pill" href="#notification" role="tab" aria-controls="notification" aria-selected="false">
                            <i class="fa fa-battery-half" aria-hidden="true"></i> 
                            Animes empezados
                        </a>
                        <a class="nav-link" id="notification-tab" data-toggle="pill" href="#despues" role="tab" aria-controls="despues" aria-selected="false">
                            <i class="fa fa-battery-empty" aria-hidden="true"></i> 
                            Ver mas tarde
                        </a><br>
                    </div>
                </div>
                <div class="tab-content p-4 p-md-5" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="account" role="tabpanel" aria-labelledby="account-tab">
                        <h3 class="mb-4">Información</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nombre</label>&nbsp;<i class="fa fa-address-book"></i>
                                            <p><?php echo $registro[3] ?></p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Apellidos</label>&nbsp;<i class="fa fa-address-book-o"></i>
                                            <p><?php echo $registro[4] ?></p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email</label>&nbsp;<i class="fa fa-envelope-open" aria-hidden="true"></i>
                                            <p><?php echo $registro[1] ?></p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Teléfono</label>&nbsp;<i class="fa fa-phone"></i>
                                            <p><?php echo $registro[5] ?></p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Twitter</label>&nbsp;<i class="fa fa-twitter"></i>
                                            <p><?php echo $registro[6] ?></p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Instagram</label>&nbsp;<i class="fa fa-instagram"></i>
                                            <p><?php echo $registro[7] ?></p>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Bio</label>&nbsp;<i class="fa fa-book"></i>
                                            <p><?php echo $registro[8]?></p>
                                        </div>
                                    </div>
                            
                                </div>
                                
                        <div>
                        </div>
                    </div>
                    
                    <div class="tab-pane fade" id="security" role="tabpanel" aria-labelledby="security-tab">
                        <h3 class="mb-4">Lista de anime</h3>
                        <div class="row">                 
                            <section class="margen">
                            <div id="div1">    
                                <div class="tbl-header">
                                    <table class="perfil" cellpadding="0" cellspacing="0" border="0">
<?php 
                if($visible=="Visible"){
?>   
                                      <thead>
                                        <tr>
                                          <th>Anime</th>
                                          <th>Puntuación</th>
                                          <th>Progreso</th>
                                          <th>Episodios</th>
                                          <th>Fecha inicio</th>
                                          <th>Ultima actualización</th>
                                          <th>Estado</th>
                                        </tr>
                                      </thead>
                                    
                                  

                                        
                                    
                                            <tbody>
                                      
<?php

                 
   
                

                    $sql100 = "SELECT NombreUsuario ,NombreAnime,Score,Progreso,Episodios,FechaInicio,UltimaActualizacion,Completo FROM listaanime WHERE NombreUsuario='$perfil'";
        
                    $result100 = mysqli_query ($conn, $sql100);
        
                    $columna=0;
                    if ($result100 == TRUE) {

                        while ($registro11 = mysqli_fetch_row($result100)) {
                            
                            if($columna%2==0){

                                $color="color1";

                            }else{

                                $color="color2";
                            } 
?>
                                                <tr>
                                                  <td class="<?php echo $color ?>"><?php echo $registro11[1] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro11[2] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro11[3] ?>/<?php echo $registro11[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro11[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro11[5] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro11[6] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro11[7] ?></td>
                                                </tr>                                                                               
<?php
                            $columna++;

                        }
  
                    }    
                }else{  
?>                
                    <p id="rojo">El usuario ha establecido esta lista como "Oculta". </p> <i class="fa fa-times" aria-hidden="true"></i>
<?php
                }
  
?>
                                            </tbody>
                                        
                                    </table>
                                  </div>
                            </div>
                            </section>
                        </div>
                       
                    </div>
                    <div class="tab-pane fade" id="application" role="tabpanel" aria-labelledby="application-tab">
                        <h3 class="mb-4">Lista de animes terminados</h3>
                          <div class="row">


                            <section class="margen">
                            <div id="div1">    
                                <div class="tbl-header">
                                    <table class="perfil" cellpadding="0" cellspacing="0" border="0">
<?php 
                if($visible=="Visible"){
?>                                           
                                      <thead>
                                        <tr>
                                          <th>Anime</th>
                                          <th>Puntuación</th>
                                          <th>Progreso</th>
                                          <th>Episodios</th>
                                          <th>Fecha inicio</th>
                                          <th>Ultima actualización</th>
                                          <th>Estado</th>
                                        </tr>
                                      </thead>
                                    
                                  

                                        
                                    
                                            <tbody>
                                      
<?php

                    $sql110 = "SELECT NombreUsuario ,NombreAnime,Score,Progreso,Episodios,FechaInicio,UltimaActualizacion,Completo FROM listaanime WHERE NombreUsuario='$perfil' AND Completo='Terminado'";
        
                    $result110 = mysqli_query ($conn, $sql110);
        
                    $columna=0;
                    if ($result110 == TRUE) {

                        while ($registro110 = mysqli_fetch_row($result110)) {
                            
                            if($columna%2==0){

                                $color="color1";

                            }else{

                                $color="color2";
                            } 
?>
                                                <tr>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[1] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[2] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[3] ?>/<?php echo $registro110[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[5] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[6] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[7] ?></td>
                                                </tr>                                                                               
<?php
                            $columna++;
                        }
  
                    }
                }else{  
?>                
                    <p id="rojo">El usuario ha establecido esta lista como "Oculta". </p> <i class="fa fa-times" aria-hidden="true"></i>
<?php
                }
   
?>
                                            </tbody>
                                        
                                    </table>
                                  </div>
                            </div>
                            </section>
                        </div>
                       
                    </div>
                    <div class="tab-pane fade" id="notification" role="tabpanel" aria-labelledby="notification-tab">
                        <h3 class="mb-4">Lista de animes empezados</h3>
                          <div class="row">


                            <section class="margen">
                            <div id="div1">    
                                <div class="tbl-header">
                                    <table class="perfil" cellpadding="0" cellspacing="0" border="0">
<?php 
                if($visible=="Visible"){
?>                                           
                                      <thead>
                                        <tr>
                                          <th>Anime</th>
                                          <th>Puntuación</th>
                                          <th>Progreso</th>
                                          <th>Episodios</th>
                                          <th>Fecha inicio</th>
                                          <th>Ultima actualización</th>
                                          <th>Estado</th>
                                        </tr>
                                      </thead>
                                    
                                  

                                        
                                    
                                            <tbody>
                                      
<?php

                    $sql110 = "SELECT NombreUsuario ,NombreAnime,Score,Progreso,Episodios,FechaInicio,UltimaActualizacion,Completo FROM listaanime WHERE NombreUsuario='$perfil' AND Completo='Viendose'";
        
                    $result110 = mysqli_query ($conn, $sql110);
        
                    $columna=0;
                    if ($result110 == TRUE) {

                        while ($registro110 = mysqli_fetch_row($result110)) {
                            
                            if($columna%2==0){

                                $color="color1";

                            }else{

                                $color="color2";
                            } 
?>
                                                <tr>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[1] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[2] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[3] ?>/<?php echo $registro110[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[5] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[6] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[7] ?></td>
                                                </tr>                                                                               
<?php
                            $columna++;
                        }
  
                    }
                }else{  
?>                
                    <p id="rojo">El usuario ha establecido esta lista como "Oculta". </p> <i class="fa fa-times" aria-hidden="true"></i>
<?php
                }
  
   
  
?>
                                            </tbody>
                                        
                                    </table>
                                  </div>
                            </div>
                            </section>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="despues" role="despues" aria-labelledby="despues-tab">
                        <h3 class="mb-4">Ver más tarde</h3>
                          <div class="row">


                            <section class="margen">
                            <div id="div1">    
                                <div class="tbl-header">
                                    <table class="perfil" cellpadding="0" cellspacing="0" border="0">
<?php 
                if($visible=="Visible"){
?>                                           
                                      <thead>
                                        <tr>
                                          <th>Anime</th>
                                          <th>Puntuación</th>
                                          <th>Progreso</th>
                                          <th>Episodios</th>
                                          <th>Fecha inicio</th>
                                          <th>Ultima actualización</th>
                                          <th>Estado</th>
                                        </tr>
                                      </thead>
                                    
                                  

                                        
                                    
                                            <tbody>
                                      
<?php

                    $sql110 = "SELECT NombreUsuario ,NombreAnime,Score,Progreso,Episodios,FechaInicio,UltimaActualizacion,Completo FROM listaanime WHERE NombreUsuario='$perfil' AND Completo='Despues'";
        
                    $result110 = mysqli_query ($conn, $sql110);
        
                    $columna=0;
                    if ($result110 == TRUE) {

                        while ($registro110 = mysqli_fetch_row($result110)) {
                            
                            if($columna%2==0){

                                $color="color1";

                            }else{

                                $color="color2";
                            } 
?>
                                                <tr>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[1] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[2] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[3] ?>/<?php echo $registro110[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[4] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[5] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[6] ?></td>
                                                  <td class="<?php echo $color ?>"><?php echo $registro110[7] ?></td>
                                                </tr>                                                                               
<?php
                            $columna++;
                        }  
                    }
                }else{  
?>                
                    <p id="rojo">El usuario ha establecido esta lista como "Oculta". </p> <i class="fa fa-times" aria-hidden="true"></i>
<?php
                }
  
      
?>                                            </tbody>
                                    </table>
                                  </div>
                            </div>
                            </section>
                        </div>
                       
                    </div>   
                </div>
            </div>
        </div>
    </section>

<?php  
       
        }
    }

?>

        <!-- Footer Section Begin -->
        <footer class="footer">
            <div class="page-up">
                <a href="#" id="scrollToTopButton"><span class="arrow_carrot-up"></span></a>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="footer__logo">
                            <a href="../index.php"><img src="../img/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="footer__nav">
                            <ul>
                            <li class="active"><a href="../index.php">Inicio</a></li>
                            <li><a href="categorias.php?categoria=<?php echo 'ultimos' ?>">Categories</a></li>
                            <li><a href="blog.php">Nuestro blog</a></li>
                            <li><a href="contacto.php">Contacto</a></li>
                            <li><a href="faq.php">FAQ</a></li>
                        </ul>
                        </div>
                    </div>
                    
                  </div>
              </div>
          </footer>
          <!-- Footer Section End -->

          <!-- Search model Begin -->
          <div class="search-model">
            <div class="h-100 d-flex align-items-center justify-content-center">
                <div class="search-close-switch"><i class="icon_close"></i></div>
                <form class="search-model-form">
                    <input type="text" id="search-input" placeholder="Search here.....">
                </form>
            </div>
        </div>
        <!-- Search model end -->

         <!-- Js Plugins -->
        <script src="../js/jquery-3.3.1.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/player.js"></script>
        <script src="../js/jquery.nice-select.min.js"></script>
        <script src="../js/mixitup.min.js"></script>
        <script src="../js/jquery.slicknav.js"></script>
        <script src="../js/owl.carousel.min.js"></script>
        <script src="../js/main.js"></script>



    </body>

    </html>