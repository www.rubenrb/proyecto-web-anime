<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anime | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="../css/plyr.css" type="text/css">
    <link rel="stylesheet" href="../css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="../css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="../css/style.css" type="text/css">
</head>

<body>

<?php

 session_start();

 function test_input($data) {
          $data = trim($data); 
          $data = stripslashes($data); 
         return $data;
    }

 $user="";
   
    if (isset($_SESSION["conectado"])){

        $user=$_SESSION["conectado"];
    }

if(!isset($_SESSION["conectado"])) {

    $userError= $passError  = $mailError= $usuarioError=$correoError=$registro="";
    $user = $pass= $email=  "";
    $usuario = $contra=$emails= false;

    if (isset($_POST['salir'])){

          session_destroy();
       

          header("location:login.php");
       }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {

           $contra1=$_POST["pas"];
           $contra2=$_POST["rpas"];
           $mail=$_POST["correo"];
           $nombre=$_POST["nombre"];

        if(!empty($contra1) && !empty($contra2)){
            if ($contra1!=$contra2) {
                $passError = "* Las contraseñas no coinciden <br>";
            }else{
                $pass = test_input($contra1);
                $contra = true;
            }
        }else{
            $passError = "* Introduce la contraseña <br>";
        }
        

        if(empty($nombre)) {
          $userError = "* Introduce un nombre de usuario <br>";
        }else{
          $user = test_input($nombre);
          $usuario = true;

        }

        if(empty($_POST["correo"])) {
          $mailError = "* Introduce un mail <br>";
        }else{
            if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                $email = test_input($mail);
                $emails = true;
            }else{
                $mailError = "* Introduce un mail válido <br>";
            }

        }      

    }


      $servername = "localhost";
      $username = "animeAdmin";
      $password = "animeAdmin";
      $dbname = "anime_db";

      $conn = mysqli_connect($servername, $username, $password,$dbname);

      if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
      }
      
    if(($usuario==true)&&($contra==true)&&($emails==true)){
        if (isset($_POST['ingresar'])){

            $pass=$_POST["pas"];
            $str2=$_POST["correo"];
            $str1=$_POST["nombre"];
            $usuLogin = strtolower($str1);
            $correo = strtolower($str2);

            $encript= hash_hmac('sha512', $pass, 'secret');

            $comprobacion1 = "SELECT NombreUsuario FROM login WHERE NombreUsuario='$usuLogin'";
            $comprobacion2 = "SELECT Mail FROM login WHERE Mail='$correo'";

            $result1 = mysqli_query ($conn, $comprobacion1);
            $result2 = mysqli_query ($conn, $comprobacion2);
            

            if(mysqli_num_rows($result1) > 0){
            
                $usuarioError = "* Nombre de usuario existente <br>";                
            }

            if(mysqli_num_rows($result2) > 0){
            
                $correoError = "* Mail existente <br>";                
            
            }else{


                $sql= "INSERT INTO login (NombreUsuario,Pass,Estado,Mail,Rol,Foto) VALUES ('$usuLogin','$encript','Activo','$correo','Usuario','https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png')";

                $result3 = mysqli_query ($conn, $sql);
                

                if ($result3 == FALSE) {
                    echo "Error al ejecutar la consulta.<br />";
                }else{
           
                     $registro = " Te has registrado correctamente <br>";    

                }

            }

        }
    }


?>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
       <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="../index.php">
                            <img src="../img/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li><a href="../index.php">Inicio</a></li>
                                <li><a href="#">Categorias <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="categorias.php?categoria=<?php echo 'Action' ?>">Acción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Martial_Arts' ?>">Artes marciales</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Sci-Fi' ?>">Ciencia ficción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Comedy' ?>">Comedia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Drama' ?>">Drama</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'School' ?>">Escolares</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Fantasy' ?>">Fantasia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Horror' ?>">Horror</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Kids' ?>">Infantil</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Magic' ?>">Magia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mecha' ?>">Mecha</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mystery' ?>">Misterio</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Music' ?>">Musical</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Parody' ?>">Parodia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Slice_of_Life' ?>">Recuentos de la vida</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Romance' ?>">Romance</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Seinen' ?>">Seinen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Shounen' ?>">Shounen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Supernatural' ?>">Sobrenatural</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Super_power' ?>">Super poderes</a></li>
                                    </ul>
                                </li>
                                <li><a href="blog.php">Nuestro blog</a></li>
                                <li><a href="contacto.php">Contactanos</a></li>
<?php
  
                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Usuario") {

?>
                                <li><a href="perfil.php?user=<?php echo $user ?>">Perfil de usuario</a></li>
<?php

                            }

                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Admin") {

?>                              
                                <li><a href="admin_menu.php">Administración</a></li>
<?php

                            }

?>

                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    
<?php
  
                    if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

?>
                        <div class="col-lg-6">
                            <div class="header__right">
                                <a href="login.php"><span class="icon_profile"></span></a>
                            </div>
                        </div>
<?php

                    }else{   

?>
                        <div class="col-lg-6">
                            <form  action="" method="post">
                                <button type="submit" class="site-btn3" name="salir">Salir</button>
                            </form>
                        </div>
<?php

                    }  

?>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Normal Breadcrumb Begin -->
    <section class="normal-breadcrumb set-bg" data-setbg="../img/normal-breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="normal__breadcrumb__text">
                        <h2>Regístrate</h2>
                        <p>Bienvenido a Anime Mix.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Normal Breadcrumb End -->

    <!-- Signup Section Begin -->
    <section class="signup spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="login__form">
                        <h3>Únete a nuestra web</h3>
                        <form action=""  method="post">
                            <span id="rojo"> <?php echo $userError;?></span>
                            <span id="rojo"> <?php echo $passError;?></span>
                            <span id="rojo"> <?php echo $mailError;?></span>
                            <span id="rojo"> <?php echo $usuarioError;?></span>
                            <span id="rojo"> <?php echo $correoError;?></span>
                            <span id="amarillo"> <?php echo $registro;?></span><br>
                            <div class="input__item">
                                <input type="text" name="correo" placeholder="Dirección de correo">
                                <span class="icon_mail"></span>
                            </div>
                            <div class="input__item">
                                <input type="text" name="nombre" placeholder="Nombre de usuario">
                                <span class="icon_profile"></span>
                            </div>
                            <div class="input__item">
                                <input type="text" name="pas" placeholder="Contraseña">
                                <span class="icon_lock"></span>
                            </div>
                            <div class="input__item">
                                <input type="text" name="rpas" placeholder="Repetir contraseña">
                                <span class="icon_lock"></span>
                            </div>
                            <button type="submit" class="site-btn" name="ingresar">Registrate</button>
                        </form>
                        <h5>¿Ya tienes una cuenta? <a href="login.php">¡Conectate!</a></h5>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="login__social__links">
                        <h3>Inicia sesión con:</h3>
                        <ul>
                            <li><a href="#" class="facebook"><i class="fa fa-facebook"></i> Sign in With Facebook</a>
                            </li>
                            <li><a href="#" class="google"><i class="fa fa-google"></i> Sign in With Google</a></li>
                            <li><a href="#" class="twitter"><i class="fa fa-twitter"></i> Sign in With Twitter</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Signup Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer">
        <div class="page-up">
            <a href="#" id="scrollToTopButton"><span class="arrow_carrot-up"></span></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer__logo">
                        <a href="../index.php"><img src="../img/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="footer__nav">
                        <ul>
                            <li class="active"><a href="../index.php">Inicio</a></li>
                            <li><a href="categorias.php?categoria=<?php echo 'ultimos' ?>">Categories</a></li>
                            <li><a href="blog.php">Nuestro blog</a></li>
                            <li><a href="contacto.php">Contacto</a></li>
                            <li><a href="faq.php">FAQ</a></li>
                        </ul>
                    </div>
                </div>
                
              </div>
          </div>
      </footer>
      <!-- Footer Section End -->

      <!-- Search model Begin -->
      <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch"><i class="icon_close"></i></div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

    <!-- Js Plugins -->
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/player.js"></script>
    <script src="../js/jquery.nice-select.min.js"></script>
    <script src="../js/mixitup.min.js"></script>
    <script src="../js/jquery.slicknav.js"></script>
    <script src="../js/owl.carousel.min.js"></script>
    <script src="../js/main.js"></script>

<?php  
       
}else{

    header("location:perfil.php?user=$user");

}

?>

</body>

</html>