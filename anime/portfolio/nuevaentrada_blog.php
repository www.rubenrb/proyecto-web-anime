<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anime | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="../css/plyr.css" type="text/css">
    <link rel="stylesheet" href="../css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="../css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="../css/style.css" type="text/css">
</head>

<body>
<?php
  

  session_start();
  $servername = "localhost";
  $username = "animeAdmin";
  $password = "animeAdmin";
  $dbname = "anime_db";

   $conn = mysqli_connect($servername, $username, $password,$dbname);

  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }
  

  $error1=false;
  $error=$subido="";


  if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if(empty($_POST["title"]) || empty($_POST["foto"]) || empty($_POST["contenido"])  ) {
        $error = "* Debe completar todos los campos <br>";
    }else{

        $error1 = true;
       
    }
  }



  if(($error1==true)){
      if (isset($_POST['subir'])){

        $f= date("Y").'-'.date("m").'-'.date("d");

        $title=$_POST["title"];
        $foto=$_POST["foto"];
        $contenido=$_POST["contenido"];
        
        $sql= "INSERT INTO noticias (id,Titulo,Contenido,foto,FechaNoticia) VALUES (NULL,'$title','$contenido','$foto','$f')";
         
         $result = mysqli_query ($conn, $sql);
         

          
        if ($result == FALSE) {
           
              echo "Error en la ejecución de la consulta.<br />";
              
        }else{
              
              $subido = " Se ha subido el post correctamente <br>"; 

        }

      }
    }

  
  ?>


    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="../index.php">
                            <img src="../img/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li><a href="../index.php">Homepage</a></li>
                                <li><a href="categories.php">Categories <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="categories.php">Categories</a></li>
                                        <li><a href="anime-details.php">Anime Details</a></li>
                                        <li><a href="anime-watching.php">Anime Watching</a></li>
                                        <li><a href="blog-details.php">Blog Details</a></li>
                                        <li><a href="signup.php">Sign Up</a></li>
                                        <li><a href="login.php">Login</a></li>
                                    </ul>
                                </li>
                                <li><a href="blog.php">Our Blog</a></li>
                                <li><a href="#">Contacts</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="header__right">
                        <a href="#" class="search-switch"><span class="icon_search"></span></a>
                        <a href="login.php"><span class="icon_profile"></span></a>
                    </div>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Normal Breadcrumb Begin -->
    <section class="normal-breadcrumb set-bg" data-setbg="../img/normal-breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="normal__breadcrumb__text">
                        <h2>Nueva entrada al blog de noticias</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Normal Breadcrumb End -->

    <!-- Login Section Begin -->
    <section class="login spad">
        <div class="container">
            <div class="login__form1">
                        <h3 class="contacto">Panel para añadir una nueva entrada al blog</h3>
                        <span id="amarillo">-Para el contenido del post se usaran las etiquetas html "p" para cada parrafo que se quiera añadir</span><br>
                        <span id="amarillo">-Si se quiere insrtar una imagen en el post debe ir dentro de las etiquetas html "br", "center", y "img" en ese orden para que la web pueda insertar el contenido adecuadamente desde la base de datos y quede estructurado.</span><br>
                        <span id="amarillo">-Para remarcar alguna parte del texto utilizar la etiqueta html "strong"</span><br>
                        <form action="" method="post">
                            <span id="rojo"> <?php echo $error;?></span>
                            <span id="amarillo"> <?php echo $subido;?></span><br>
                            
                            <div class="input__item">
                                <input type="text" name="title" placeholder="Titulo del post">
                                <span class="icon_search_alt">
                            </div>
                                </span>
                                <div class="input__item">
                                <input type="url" name="foto" placeholder="URL de la imagen">
                                <span class="icon_images">
                            </div>
                                </span>
                            <div class="input__item1">
                                <textarea  name="contenido" rows="6" cols="66" placeholder="Contenido del post"></textarea>
                            </div>
                           
                            
                           
                            <button type="submit" class="site-btn1" name="subir">Subir post</button>
                        </form>
                    </div>
                
            
        </div>
    </section>
    <!-- Login Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer">
        <div class="page-up">
            <a href="#" id="scrollToTopButton"><span class="arrow_carrot-up"></span></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer__logo">
                        <a href="../index.php"><img src="img/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="footer__nav">
                        <ul>
                            <li class="active"><a href="../index.php">Homepage</a></li>
                            <li><a href="./categories.php">Categories</a></li>
                            <li><a href="./blog.php">Our Blog</a></li>
                            <li><a href="#">Contacts</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3">
                    <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                      Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                      <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>

                  </div>
              </div>
          </div>
      </footer>
      <!-- Footer Section End -->


      <!-- Search model Begin -->
      <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch"><i class="icon_close"></i></div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

    <!-- Js Plugins -->
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/player.js"></script>
    <script src="../js/jquery.nice-select.min.js"></script>
    <script src="../js/mixitup.min.js"></script>
    <script src="../js/jquery.slicknav.js"></script>
    <script src="../js/owl.carousel.min.js"></script>
    <script src="../js/main.js"></script>


</body>

</html>