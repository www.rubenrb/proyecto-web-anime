<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anime | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="../css/plyr.css" type="text/css">
    <link rel="stylesheet" href="../css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="../css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="../css/style.css" type="text/css">
</head>

<body>

<?php

   session_start();

  $user="";
   
    if (isset($_SESSION["conectado"])){

        $user=$_SESSION["conectado"];
    }

  $servername = "localhost";
  $username = "animeAdmin";
  $password = "animeAdmin";
  $dbname = "anime_db";

   $conn = mysqli_connect($servername, $username, $password,$dbname);

  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }
  
    if (isset($_POST['salir'])){

      session_destroy();
   

      header("location:login.php");
   }

    $record_per_page = 12;
    $pagina = '';

    if(isset($_GET["pagina"])){
        $pagina = $_GET["pagina"];
    }
    else{

        $pagina = 1;
    }

    $start_from = ($pagina-1)*$record_per_page;
    

  
?>


    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="../index.php">
                            <img src="../img/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li><a href="../index.php">Inicio</a></li>
                                <li><a href="#">Categorias <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="categorias.php?categoria=<?php echo 'Action' ?>">Acción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Martial_Arts' ?>">Artes marciales</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Sci-Fi' ?>">Ciencia ficción</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Comedy' ?>">Comedia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Drama' ?>">Drama</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'School' ?>">Escolares</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Fantasy' ?>">Fantasia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Horror' ?>">Horror</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Kids' ?>">Infantil</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Magic' ?>">Magia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mecha' ?>">Mecha</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Mystery' ?>">Misterio</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Music' ?>">Musical</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Parody' ?>">Parodia</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Slice_of_Life' ?>">Recuentos de la vida</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Romance' ?>">Romance</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Seinen' ?>">Seinen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Shounen' ?>">Shounen</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Supernatural' ?>">Sobrenatural</a></li>
                                        <li><a href="categorias.php?categoria=<?php echo 'Super_power' ?>">Super poderes</a></li>
                                    </ul>
                                </li>
                                <li class="active"><a href="blog.php">Nuestro blog</a></li>
                                <li><a href="contacto.php">Contactanos</a></li>
<?php
  
                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Usuario") {

?>
                                <li><a href="perfil.php?user=<?php echo $user ?>">Perfil de usuario</a></li>
<?php

                            }

                            if(isset($_SESSION["conectado"]) && ($_SESSION["rol"])=="Admin") {

?>                              
                                <li><a href="admin_menu.php">Administración</a></li>
<?php

                            }

?>

                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    
<?php
  
                    if(!isset($_SESSION["conectado"]) && !isset($_SESSION["rol"])) {

?>
                        <div class="col-lg-6">
                            <div class="header__right">
                                <a href="login.php"><span class="icon_profile"></span></a>
                            </div>
                        </div>
<?php

                    }else{   

?>
                        <div class="col-lg-6">
                            <form  action="#" method="post">
                                <button type="submit" class="site-btn3" name="salir">Salir</button>
                            </form>
                        </div>
<?php

                    }  

?>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Normal Breadcrumb Begin -->
    <section class="normal-breadcrumb set-bg" data-setbg="../img/normal-breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="normal__breadcrumb__text">
                        <h2>Nuestro blog</h2>
                        <p>Últimas noticias relacionadas con el anime.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Normal Breadcrumb End -->


    <!-- Blog Section Begin -->
    <section class="blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
<?php

                            $sql = " SELECT id,Titulo,foto,FechaNoticia FROM noticias ORDER BY id DESC LIMIT $start_from, $record_per_page";

                            $result = mysqli_query($conn, $sql);

                            if ($result == TRUE) {

                                while ($registro = mysqli_fetch_row($result)) {

                                    $id=$registro[0];
?>
                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                        <div class="blog__item small__item set-bg" data-setbg="<?php echo $registro[2] ?>">
                                            <div class="blog__item__text">
                                                <p class="colorfecha"><span class="icon_calendar"></span> <?php echo $registro[3] ?></p>
                                                <h4><a href="post_blog.php?id=<?php echo $registro[0] ?>"><?php echo $registro[1] ?></a></h4>
                                            </div>
                                        </div>
                                    </div>
<?php
                                }
                            }else{
                                echo "No se han encontrado Registros.";
                            }
?>
                    </div>
                </div>
            </div>
<?php                    

                            $sql1 = " SELECT id,Titulo,foto,FechaNoticia FROM noticias ORDER BY id DESC";

                            $result1 = mysqli_query($conn, $sql1);


                            $total_records = mysqli_num_rows($result1);

                            $total_pages = ceil($total_records/$record_per_page);
                            $start_loop = $pagina;
                            $diferencia = $total_pages - $pagina;

                            if($diferencia <= 12){

                             $start_loop = $total_pages - 6;

                            }

                            $end_loop = $start_loop + 5;

                            if($pagina > 1){

                             echo "<a class='pagina' href='blog.php?pagina=1'>Primera</a>";
                             echo "<a class='pagina' href='blog.php?pagina=".($pagina - 1)."'><<</a>";

                            }

                            for($i=$start_loop; $i<=$end_loop; $i++){ 

                                if($i>=1){    

                                    echo "<a class='pagina' href='blog.php?pagina=".$i."'>".$i."</a>";
                                }
                            }

                            if($pagina <= $end_loop){

                             echo "<a class='pagina' href='blog.php?pagina=".($pagina + 1)."'>>></a>";
                             echo "<a class='pagina' href='blog.php?pagina=".$total_pages."'>Última</a>";

                            }


?>
                        
                    
        </div>
    </section>
    <!-- Blog Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer">
        <div class="page-up">
            <a href="#" id="scrollToTopButton"><span class="arrow_carrot-up"></span></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer__logo">
                        <a href="../index.php"><img src="../img/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="footer__nav">
                        <ul>
                            <li class="active"><a href="../index.php">Inicio</a></li>
                            <li><a href="categorias.php?categoria=<?php echo 'ultimos' ?>">Categories</a></li>
                            <li><a href="blog.php">Nuestro blog</a></li>
                            <li><a href="contacto.php">Contacto</a></li>
                            <li><a href="faq.php">FAQ</a></li>
                        </ul>
                    </div>
                </div>
                
              </div>
          </div>
      </footer>
      <!-- Footer Section End -->

      <!-- Search model Begin -->
      <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch"><i class="icon_close"></i></div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

    <!-- Js Plugins -->
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/player.js"></script>
    <script src="../js/jquery.nice-select.min.js"></script>
    <script src="../js/mixitup.min.js"></script>
    <script src="../js/jquery.slicknav.js"></script>
    <script src="../js/owl.carousel.min.js"></script>
    <script src="../js/main.js"></script>

</body>

</html>